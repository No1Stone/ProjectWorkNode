package com.ui.projectworknode.project.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectBookmark is a Querydsl query type for ProjectBookmark
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectBookmark extends EntityPathBase<ProjectBookmark> {

    private static final long serialVersionUID = -54524389L;

    public static final QProjectBookmark projectBookmark = new QProjectBookmark("projectBookmark");

    public final StringPath bookmarkYn = createString("bookmarkYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProjectBookmark(String variable) {
        super(ProjectBookmark.class, forVariable(variable));
    }

    public QProjectBookmark(Path<? extends ProjectBookmark> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectBookmark(PathMetadata metadata) {
        super(ProjectBookmark.class, metadata);
    }

}

