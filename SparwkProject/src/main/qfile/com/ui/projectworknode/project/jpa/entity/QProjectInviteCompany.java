package com.ui.projectworknode.project.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectInviteCompany is a Querydsl query type for ProjectInviteCompany
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectInviteCompany extends EntityPathBase<ProjectInviteCompany> {

    private static final long serialVersionUID = 304004975L;

    public static final QProjectInviteCompany projectInviteCompany = new QProjectInviteCompany("projectInviteCompany");

    public final NumberPath<Long> compProfileId = createNumber("compProfileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProjectInviteCompany(String variable) {
        super(ProjectInviteCompany.class, forVariable(variable));
    }

    public QProjectInviteCompany(Path<? extends ProjectInviteCompany> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectInviteCompany(PathMetadata metadata) {
        super(ProjectInviteCompany.class, metadata);
    }

}

