package com.ui.projectworknode.project.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectReferenceFile is a Querydsl query type for ProjectReferenceFile
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectReferenceFile extends EntityPathBase<ProjectReferenceFile> {

    private static final long serialVersionUID = -1255181918L;

    public static final QProjectReferenceFile projectReferenceFile = new QProjectReferenceFile("projectReferenceFile");

    public final NumberPath<Long> fileFefSeq = createNumber("fileFefSeq", Long.class);

    public final StringPath fileName = createString("fileName");

    public final StringPath filePath = createString("filePath");

    public final NumberPath<Integer> fileSize = createNumber("fileSize", Integer.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final StringPath refUrl = createString("refUrl");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath uploadTypeCd = createString("uploadTypeCd");

    public QProjectReferenceFile(String variable) {
        super(ProjectReferenceFile.class, forVariable(variable));
    }

    public QProjectReferenceFile(Path<? extends ProjectReferenceFile> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectReferenceFile(PathMetadata metadata) {
        super(ProjectReferenceFile.class, metadata);
    }

}

