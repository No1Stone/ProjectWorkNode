package com.ui.projectworknode.project.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectMetadata is a Querydsl query type for ProjectMetadata
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectMetadata extends EntityPathBase<ProjectMetadata> {

    private static final long serialVersionUID = 1785060372L;

    public static final QProjectMetadata projectMetadata = new QProjectMetadata("projectMetadata");

    public final StringPath attrDtlCd = createString("attrDtlCd");

    public final StringPath attrTypeCd = createString("attrTypeCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProjectMetadata(String variable) {
        super(ProjectMetadata.class, forVariable(variable));
    }

    public QProjectMetadata(Path<? extends ProjectMetadata> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectMetadata(PathMetadata metadata) {
        super(ProjectMetadata.class, metadata);
    }

}

