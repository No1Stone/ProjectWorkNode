package com.ui.projectworknode.project.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProjectServiceCountry is a Querydsl query type for ProjectServiceCountry
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProjectServiceCountry extends EntityPathBase<ProjectServiceCountry> {

    private static final long serialVersionUID = 586777670L;

    public static final QProjectServiceCountry projectServiceCountry = new QProjectServiceCountry("projectServiceCountry");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> projId = createNumber("projId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath serviceCntrCd = createString("serviceCntrCd");

    public QProjectServiceCountry(String variable) {
        super(ProjectServiceCountry.class, forVariable(variable));
    }

    public QProjectServiceCountry(Path<? extends ProjectServiceCountry> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProjectServiceCountry(PathMetadata metadata) {
        super(ProjectServiceCountry.class, metadata);
    }

}

