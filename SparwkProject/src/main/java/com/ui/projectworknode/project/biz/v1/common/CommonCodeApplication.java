package com.ui.projectworknode.project.biz.v1.common;

import com.ui.projectworknode.project.biz.v1.common.dto.AttrDtlCdResponse;
import com.ui.projectworknode.project.biz.v1.common.dto.CompanyResponse;
import com.ui.projectworknode.project.biz.v1.common.dto.LanguageCdResponse;
import com.ui.projectworknode.project.biz.v1.common.dto.ProfileResponse;
import com.ui.projectworknode.project.jpa.repository.NativeRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommonCodeApplication {


    private final NativeRepository nativeRepository;
    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(CommonCodeApplication.class);
    private final HttpServletRequest request;




    /**
     * cowriter Split sum
     * @return
     * when 0 , leave project
     * when better then 0 , don't leave your project
     */
    public boolean cowriterSplitRateSum(Long projId, Long profileId) {
        logger.info("<<<<< cowriterSplitRateSum {} ", projId);
        return nativeRepository.cowriterSplitRateSum(projId, profileId)
                .stream().mapToLong(e -> e).sum() > 0 ? false : true;
    }

    /**
     * only one
     * @param dcode
     * @return
     */
    public String getCodeName(String dcode) {
        logger.info("<<<<< getCodeName {} ", dcode);
        if(StringUtils.isEmpty(dcode)) {
            return null;
        }
        return nativeRepository.getCodeName(dcode);
    }

    /**
     * only one
     * @param dcodes
     * @return
     */
    public List<AttrDtlCdResponse> getAttrDtlCdValList(List<String> dcodes) {
        logger.info("<<<<< getAttrDtlCdValList {} ", dcodes);
        if(dcodes == null && dcodes.size() < 0) {
            return null;
        }
        return nativeRepository.getAttrDtlCdValList(dcodes);
    }


    /**
     * only one
     * @param dcodes
     * @return
     */
    public List<AttrDtlCdResponse> getRoleValList(List<String> dcodes) {
        logger.info("<<<<< getAttrDtlCdValList {} ", dcodes);
        if(dcodes == null && dcodes.size() < 0) {
            return null;
        }
        return nativeRepository.getRoleValList(dcodes);
    }

    /**
     * only one
     * @param dcodes
     * @return
     */
    public List<AttrDtlCdResponse> getSongCdValList(List<String> dcodes) {
        logger.info("<<<<< getSongCdValList {} ", dcodes);
        if(dcodes == null && dcodes.size() < 0) {
            return null;
        }
        return nativeRepository.getSongCdValList(dcodes);
    }


    /**
     * only one
     * @param dcodes
     * @return
     */
    public List<LanguageCdResponse> getLanguageValList(List<String> dcodes) {
        logger.info("<<<<< getLanguageValList {} ", dcodes);
        if(dcodes == null && dcodes.size() < 0) {
            return null;
        }
        return nativeRepository.getLanguageValList(dcodes);
    }

    /**
     * profile Info
     * @param profileId
     * @return
     */
    public ProfileResponse findViewUserInfo(Long profileId) {
        logger.info("<<<<< findViewUserInfo {} ", profileId);
        ProfileResponse result = nativeRepository.findProfileInfo(profileId);
        if(result == null) {
            return null;
        }
        return result;
    }

    /**
     * company profile detail info
     * @param profileId
     * @return
     */
    public CompanyResponse findCompanyInfoDetail(Long profileId) {
        logger.info("<<<<< findViewUserInfo {} ", profileId);
        CompanyResponse result = nativeRepository.findCompanyInfoDetail(profileId);
        if(result == null) {
            return null;
        }
        return result;
    }


    /**
     * project owner change 시 pitched 된 song 체크
     * when count > 0 then true - 오너 체인지 불가능
     * when count <=0 then false - 오너 체인지 가능
     * @param projId
     * @return
     */
    public boolean countProjectPitchedSong(Long projId) {
        logger.info("<<<<<< countProjectPitchedSong {}", projId);
        return nativeRepository.countProjectPitchedSong(projId) > 0 ? true : false;
    }


    /**
     * song owner change
     * @param projOwner
     * @param projId
     * @return
     */
    public void updateSongOwnerChange(Long projOwner, Long projId) {
        logger.info("<<<<<< updateSongOwnerChange {}", projId);
        nativeRepository.updateSongOwnerChange(projOwner, projId);
    }

    /**
     * song owner change
     * @param projId
     * @param profileId
     * @return
     */
    public void deleteSongCowriter(Long projId, Long profileId) {
        logger.info("<<<<<< deleteSongCowriter {}", projId);
        nativeRepository.deleteSongCowriter(projId, profileId);
    }

    /**
     * song owner change
     * @param projId
     * @param profileId
     * @return
     */
    public void deleteSongCredit(Long projId, Long profileId) {
        logger.info("<<<<<< deleteSongCowriter {}", projId);
        nativeRepository.deleteSongCredit(projId, profileId);
    }


}
