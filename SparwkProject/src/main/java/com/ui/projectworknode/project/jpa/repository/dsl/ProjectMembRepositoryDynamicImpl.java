package com.ui.projectworknode.project.jpa.repository.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.ui.projectworknode.project.biz.v1.member.dto.ProjectMembResponse;
import com.ui.projectworknode.project.constants.CommonCodeConst;
import com.ui.projectworknode.project.jpa.entity.QProject;
import com.ui.projectworknode.project.jpa.entity.QProjectMemb;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

@Aspect
public class ProjectMembRepositoryDynamicImpl implements ProjectMembRepositoryDynamic{
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private QProject qProject = QProject.project;
    private final Logger logger = LoggerFactory.getLogger(ProjectMembRepositoryDynamicImpl.class);

    private ProjectMembRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public List<ProjectMembResponse> projectMembList(Long projId) {
        logger.info("### projectMembList ### {}" ,projId);

        QProjectMemb qProjectMemb = QProjectMemb.projectMemb;

        List<ProjectMembResponse> projectMembs =  queryFactory
                .select(Projections.bean(ProjectMembResponse.class
                        ,qProjectMemb.projId
                        ,qProjectMemb.profileId
                        ,qProjectMemb.confirmYn
                        ,qProjectMemb.applyStat
                        ,qProjectMemb.invtStat
                        )
                )
                .from(qProjectMemb)
                .where(qProjectMemb.projId.eq(projId)
                .and(qProjectMemb.activeYn.eq(CommonCodeConst.YN.Y))
                ).fetch();
        //TODO : result 값에 따라 res 결과 반환
        em.flush();
        em.clear();
        return projectMembs;
    }
}
