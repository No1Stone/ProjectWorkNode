package com.ui.projectworknode.project.biz.v1.project.controller;

import com.ui.projectworknode.project.biz.v1.project.dto.ProjectBookmarkRequest;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileDTO;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFilesModifyRequest;
import com.ui.projectworknode.project.biz.v1.project.dto.metadata.ProjectMetaDataDTO;
import com.ui.projectworknode.project.biz.v1.project.dto.project.*;
import com.ui.projectworknode.project.biz.v1.project.service.ProjectService;
import com.ui.projectworknode.project.config.common.Environment;
import com.ui.projectworknode.project.config.common.Response;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = Environment.API_VERSION)
@Api(tags = "New Project API")
public class ProjectController {

    private final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    private final ProjectService projectServiceRepository;

    @ApiOperation(
            value = "프로젝트 sequence 생성"
            ,tags = {"(0800) Create Project - 1st"}
            ,notes = "tb_project"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/init/seq")
    public Response<Long> initProjSeq() {
        return projectServiceRepository.initProjSeq();
    }

    @ApiOperation(
            value = "프로젝트 생성",
            notes = "tb_project, tb_project_memb, tb_project_invite_company, tb_project_metadata, tb_project_service_country, tb_project_reference_file",
            tags = {"(0800) Create Project - 1st"}
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5110, message = "실패"),
            @ApiResponse(code=5130, message = "유효하지 않은 회사코드")
    })
    @PostMapping(path = "/save")
    public Response<Long> saveProject(
            @RequestBody ProjectSaveRequest projectSaveRequest) {
        return projectServiceRepository.projectSaveService(projectSaveRequest);
    }

    @ApiOperation(
            value = "프로젝트 리스트 조회"
            ,notes = "tb_project\n" +
            ",tb_project_memb\n" +
            ",tb_project_bookmark"
            ,tags = {"(1300) Main Dashboard","(0411) ProjectViewmode"}
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "profileId", value = "프로젝트 소유자", dataType = "Long", paramType = "path", required = true)
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5100, message = "프로젝트가 존재하지 않음"),
    })
    @GetMapping(path = "/list/{profileId}")
    public Response<List<ProjectResponse>> getProjectList(
            @PathVariable(name="profileId") Long profileId,
            ProjectFilter projectFilter
    ) {
        logger.info("######### getProjectList #######");
        return projectServiceRepository.getProjectList(profileId, projectFilter);
    }


    @ApiOperation(
            value = "프로젝트 상세 데이터"
            ,notes = "tb_project\n" +
            ",tb_project_memb\n" +
            ",tb_project_bookmark"
            ,tags = {"(0900) Project detail (for Owner)"}
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projId", value = "프로젝트 번호", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "profileId", value = "프로젝트 번호", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5000, message = "프로젝트의 멤버가 존재하지 않음"),
            @ApiResponse(code=5100, message = "프로젝트가 존재하지 않음"),
    })
    @PostMapping(path = "/detail/{projId}/{profileId}")
    public Response<ProjectBaseDTO> getProjectDetail(
            @PathVariable(name="projId") long projId,
            @PathVariable(name="profileId") long profileId
    ) {
        logger.info("######### getProjectOne #######");
        return projectServiceRepository.getProjectDetail(projId ,profileId);
    }

    @ApiOperation(
            value = "프로젝트 상세 메타데이터",
            notes = "tb_project_metadata",
            tags = {"(0900) Project detail (for Owner)"}
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projId", value = "프로젝트 번호", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5100, message = "프로젝트가 존재하지 않음"),
    })
    @PostMapping(path = "/metadata/{projId}")
    public Response<List<ProjectMetaDataDTO>> getProjectMetadata(
            @PathVariable(name="projId") long projId
    ) {
        logger.info("######### getProjectOne #######");
        return projectServiceRepository.getProjectMetadata(projId);
    }


    @ApiOperation(
            value = "프로젝트 file upload 리스트",
            notes = "tb_project, tb_project_reference_file",
            tags = {"(0900) Project detail (for Owner)"}
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projId", value = "프로젝트 번호", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5100, message = "프로젝트가 존재하지 않음"),
    })
    @PostMapping(path = "/file-list/{projId}")
    public Response<List<ProjectReferenceFileDTO>> getProjectReferenceFileList(
            @PathVariable(name="projId") Long projId
    ) {
        logger.info("######### getFilesInfo #######");
        return projectServiceRepository.getProjectReferenceFileList(projId);
    }

    @ApiOperation(
            value = "프로젝트 수정",
            notes = "tb_project\n" +
                    ",tb_project_metadata\n" +
                    ",tb_project_reference_file",
            tags = {"(0903) RSVP Info (for Owner)"}
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=2000, message = "수정 실패"),
    })
    @PostMapping(path = "/modify")
    public Response<Void> projectDynamicUpdateController(
            @RequestBody ProjectModifyRequest dto
    ) {
        logger.info("###### projectDynamicUpdateController ######");
        return projectServiceRepository.updateProjectDetail(dto);
    }

    @ApiOperation(
            value = "프로젝트 owner change",
            notes = "tb_project",
            tags = {"(0900) Project detail (for Owner)"}
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=2000, message = "수정 실패"),
    })
    @PostMapping(path = "/owner/change")
    public Response<Void> projectOwnerChange(
            @RequestParam(name = "projId") Long projId
            ,@RequestParam(name = "projOwner") Long projOwner
    ) {
        logger.info("###### projectDynamicUpdateController ######");
        return projectServiceRepository.projectOwnerChange(projId, projOwner);
    }


    @ApiOperation(
            value = "프로젝트 파일 수정",
            notes = "tb_project_reference_file",
            tags = {"(0900) Project detail (for Owner)"}
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=9999, message = "에러"),
    })
    @PostMapping(path = "/modify/files")
    public Response<Void> projectFilesUpdate(
            @RequestBody ProjectReferenceFilesModifyRequest dto
            ) {
        logger.info("###### projectFilesUpdate ######");
        return projectServiceRepository.updateProjectFiles(dto);
    }


    @ApiOperation(
            value = "프로젝트 북마크",
            notes = "tb_project\n" +
                    ",tb_project_bookmark",
            tags = {"(1300) Main Dashboard","(0411) ProjectViewmode"}
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5010, message = "project Id가 존재하지 않음"),
    })
    @PostMapping(path = "/bookmark")
    public Response<Void> projectBookmark(
            @RequestBody ProjectBookmarkRequest projectBookmarkRequest
    ) {
        logger.info("###### projectBookmark ######");
        return projectServiceRepository.updateProjectBookmark(projectBookmarkRequest);
    }

}
