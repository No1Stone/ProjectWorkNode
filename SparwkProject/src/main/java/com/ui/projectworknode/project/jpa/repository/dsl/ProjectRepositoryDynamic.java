package com.ui.projectworknode.project.jpa.repository.dsl;

import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectBaseDTO;
import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectFilter;
import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectRequest;
import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectResponse;

import java.util.List;

public interface ProjectRepositoryDynamic {

    long projectRepositoryDynamicUpdate(ProjectBaseDTO DTO);
    long updateprojectOwnerChange(ProjectBaseDTO dto);
    List<ProjectResponse> projectListDynamic(ProjectRequest req, ProjectFilter projectFilter);
}
