package com.ui.projectworknode.project.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectServiceCountryId implements Serializable {
    private long projId;
    private String serviceCntrCd;
}
