//package com.ui.projectworknode.project.config;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//@EnableWebSecurity
//@RequiredArgsConstructor
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
////	final UserTokenProvider userTokenProvider;
////	final FranchiseeTokenProvider franchiseeTokenProvider;
////	final AuthenticationSuccessHandler authenticationSuccessHandler;
////	final AuthenticationFailureHandler authenticationFailureHandler;
////
////	final AuthenticationProvider authenticationProvider;
////	final UserDetailsService userDetailsService;
////	final PasswordEncoder passwordEncoder;
////
////	final CommonService commonService;
////
////	@Value("${spring.security.user.name}")
////	private String username;
////
////	@Value("${spring.security.user.password}")
////	private String password;
////	//UserTokenProvider
////	//정적자원에 대한 security 해제
////
////
////
////	@Override
////	public void configure(WebSecurity web) throws Exception {
////		web.httpFirewall(defaultHttpFirewall());
////		web.ignoring().antMatchers("/resources/**","/uploads/**","/swagger-ui.html",
////				"/swagger-resources/**","/v2/api-docs","/swagger-ui","/swagger-ui/**","/favicon.ico"
////				,"/web/**"
////				,"/test/**"
////				,"/api/v1/open-banking/**"
////				,"/api/v1/account/login"
////				,"/api/v1/account/join"
////				,"/api/v1/account/social/**"
////				,"/api/v1/account/valid/**"
////				,"/api/v1/account/find/**"
////				,"/api/v1/account/sms/**"
////				,"/auth/**"
////				,"/api/v1/account/web/token/authorize"
////				,"/api/v1/account/web/token"
////				,"/api/v1/account/iss/**"
////				,"/api/v1/campaign/release/**"
////				,"/api/v1/campaign/pre/auth/fail"
////				,"/api/v1/alarm/check/appver"
////				,"/api/f1/frchs/login"
////				,"/api/f1/frchs/login/second"
////				,"/api/f1/frchs/refresh/authorize"
////		);
////	}
////
////	/* security 기본 설정 */
////	@Override
////	public void configure(HttpSecurity http) throws Exception {
////		http
////				.cors().and()
////				.csrf().disable()
////				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
////				.and()
////				.addFilter(new JwtAuthorizationFilter(authenticationManager(), userTokenProvider ,franchiseeTokenProvider, commonService))
////				.authorizeRequests()
////				.antMatchers("/api/*/account/*","/web/*/auth/*","/","/login"
////						,"/test/**"
////						,"/api/v1/pointpay/sample"
////						,"/api/v1/open-banking/**"
////						,"/api/v1/deferred/pay"
////						,"/web/**"
////						,"/api/v1/account/login"
////						,"/api/v1/account/join"
////						,"/api/v1/account/social/**"
////						,"/api/v1/account/valid/**"
////						,"/api/v1/account/find/**"
////						,"/api/v1/account/sms/**"
////						,"/auth/**"
////						,"/api/v1/account/refresh/authorize"
////						,"/api/v1/account/web/token/authorize"
////						,"/api/v1/account/web/token"
////						,"/api/v1/account/iss/**"
////						,"/api/v1/campaign/release/**"
////						,"/api/v1/campaign/pre/auth/fail"
////						,"/api/f1/frchs/login"
////						,"/api/f1/frchs/login/second"
////						,"/api/f1/frchs/refresh/authorize"
////						,"/api/f1/second/**"
////				)
////				.permitAll()
////				.antMatchers("/swagger-ui/**", "/error").permitAll()
////				.antMatchers("/**").authenticated();
////
////
////	}
////	@Override
////	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
////		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
////	}
////
////	@Bean
////	public CorsConfigurationSource corsConfigurationSource() {
////		CorsConfiguration configuration = new CorsConfiguration();
////
////		configuration.addAllowedOrigin("*");
////		configuration.addAllowedHeader("*");
////		configuration.addAllowedMethod("*");
////		configuration.setAllowCredentials(true);
////
////		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
////		source.registerCorsConfiguration("/**", configuration);
////		return source;
////	}
////
////	@Bean
////	public HttpFirewall defaultHttpFirewall() {
////		return new DefaultHttpFirewall();
////	}
//
//}