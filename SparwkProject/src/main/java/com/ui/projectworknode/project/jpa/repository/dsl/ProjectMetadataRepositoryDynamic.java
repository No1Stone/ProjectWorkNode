package com.ui.projectworknode.project.jpa.repository.dsl;

import com.ui.projectworknode.project.biz.v1.project.dto.metadata.ProjectMetaResponse;
import com.ui.projectworknode.project.jpa.entity.ProjectMetadata;

import java.util.List;

public interface ProjectMetadataRepositoryDynamic {

    long projectMetadataDynamicDelete(ProjectMetadata DTO);
    List<ProjectMetaResponse> projectMetadataDetailDynamic(Long projId);
    List<ProjectMetaResponse> projectMetadataListDynamic(Long projId);

}
