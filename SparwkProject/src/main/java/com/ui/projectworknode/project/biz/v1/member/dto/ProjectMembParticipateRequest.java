package com.ui.projectworknode.project.biz.v1.member.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMembParticipateRequest {

    @ApiModelProperty(value = "ownerProfileId", notes = "ownerProfileId" , example = "")
    @Schema(name = "ownerProfileId", description = "ownerProfileId parameter", example = "")
    private Long ownerProfileId;

    @ApiModelProperty(value = "projectmember basic info", notes = "프로잭트 member projId, profileId" , example = "")
    @Schema(name = "projectMember", description = "projectMembRequest parameter", example = "")
    private ProjectMembRequest projectMember;

    @ApiModelProperty(value = "초대/지원", notes = "프로잭트 member 초대/지원" , example = "RSVP/APPLY")
    @Schema(name = "type", description = "project member participate type", example = "RSVP/APPLY")
    private String type;
}
