package com.ui.projectworknode.project.biz.v1.common.dto;

public interface LanguageCdResponse {

    String getLanguageCd();
    String getLanguage();
}
