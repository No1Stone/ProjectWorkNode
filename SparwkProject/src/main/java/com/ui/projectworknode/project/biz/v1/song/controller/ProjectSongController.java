package com.ui.projectworknode.project.biz.v1.song.controller;

import com.ui.projectworknode.project.biz.v1.song.dto.ProjectSongRequest;
import com.ui.projectworknode.project.biz.v1.song.service.ProjectSongService;
import com.ui.projectworknode.project.config.common.Environment;
import com.ui.projectworknode.project.config.common.Response;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = Environment.API_VERSION + "/project-song")
@Api(tags = "New SONG API")
public class ProjectSongController {

    private final Logger logger = LoggerFactory.getLogger(ProjectSongController.class);

    private final ProjectSongService projectSongServiceRepository;

    @ApiOperation(
            value = "노래 생성시 project-song 연결"
            ,notes = "tb_project,tb_proejct_song"
            ,tags = {"(0901) New Song"}
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=1010, message = "project Id가 존재하지 않음"),
            @ApiResponse(code=9999, message = "ERROR")
    })
    @PostMapping(path = "/save")
    public Response<Void> saveProjectSong(
            @RequestBody ProjectSongRequest projectSongRequest
            ) {
        logger.info("#### saveProjectSong");
        return projectSongServiceRepository.saveProjectSong(projectSongRequest);
    }

}
