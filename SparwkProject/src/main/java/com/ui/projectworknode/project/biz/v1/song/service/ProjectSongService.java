package com.ui.projectworknode.project.biz.v1.song.service;

import com.ui.projectworknode.project.biz.v1.song.dto.ProjectSongRequest;
import com.ui.projectworknode.project.config.common.Response;
import com.ui.projectworknode.project.constants.ResultCodeConst;
import com.ui.projectworknode.project.jpa.entity.Project;
import com.ui.projectworknode.project.jpa.entity.ProjectSong;
import com.ui.projectworknode.project.jpa.repository.ProjectRepository;
import com.ui.projectworknode.project.jpa.repository.ProjectSongRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProjectSongService {
    private final Logger logger = LoggerFactory.getLogger(ProjectSongService.class);

    private final ModelMapper modelMapper;

    private final ProjectRepository projectRepository;
    private final ProjectSongRepository projectSongRepository;
    private final MessageSourceAccessor messageSourceAccessor;


    /**
     * saveProjectSong
     * @param projectSongRequest
     * @return
     */
    @Transactional
    public Response<Void> saveProjectSong(ProjectSongRequest projectSongRequest) {
        logger.info("##### saveProjectSong #####");
        Response<Void> res = new Response<>();

        if(projectSongRequest == null) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        Optional<Project> projectResult =  projectRepository.findById(projectSongRequest.getProjId());

        if(projectResult.isEmpty()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        ProjectSong projectSong = ProjectSong.builder()
                .projId(projectSongRequest.getProjId())
                .songId(projectSongRequest.getSongId())
                .regUsr(projectSongRequest.getProjOwner())
                .regDt(LocalDateTime.now())
                .modUsr(projectSongRequest.getProjOwner())
                .modDt(LocalDateTime.now())
                .build();

        try {
            projectSongRepository.save(projectSong);
        } catch (Exception e){
            logger.error("projectReferenceFileDTOResponse ERROR ####");
            res.setResultCd(ResultCodeConst.ERROR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
