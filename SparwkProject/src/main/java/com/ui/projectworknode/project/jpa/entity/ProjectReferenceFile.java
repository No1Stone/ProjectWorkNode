package com.ui.projectworknode.project.jpa.entity;

import com.ui.projectworknode.project.jpa.entity.id.ProjectReferenceFileId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(ProjectReferenceFileId.class)
@Table(name = "tb_project_reference_file")
public class ProjectReferenceFile  {

    @Id
    @Column(name = "proj_id", nullable = false)
    private Long projId;
    @Id
    @GeneratedValue(generator = "tb_project_reference_file_seq")
    @Column(name = "file_fef_seq", nullable = false)
    private Long fileFefSeq;
    @Column(name = "upload_type_cd", nullable = false)
    private String uploadTypeCd;
    @Column(name = "file_name", nullable = false)
    private String fileName;
    @Column(name = "file_path", nullable = false)
    private String filePath;
    @Column(name = "file_size", nullable = false)
    private int fileSize;
    @Column(name = "ref_url", nullable = false)
    private String refUrl;

    @Column(name = "reg_usr", nullable = false)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = false)
    @CreatedDate//serverTime
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = false)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = false)
    @LastModifiedDate//serverTime
    private LocalDateTime modDt;

    @Builder
    ProjectReferenceFile(
        Long projId,
        Long fileFefSeq,
        String uploadTypeCd,
        String fileName,
        String filePath,
        int fileSize,
        String refUrl,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
        this.projId = projId;
        this.fileFefSeq = fileFefSeq;
        this.uploadTypeCd = uploadTypeCd;
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileSize = fileSize;
        this.refUrl = refUrl;
        this.regUsr= regUsr;
        this.regDt= regDt;
        this.modUsr= modUsr;
        this.modDt= modDt;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;

}
