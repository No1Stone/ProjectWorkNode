package com.ui.projectworknode.project.config.filter.token;

import lombok.*;

@Getter@Setter@Builder
@NoArgsConstructor@AllArgsConstructor
public class Token {

    private UserInfoDTO userInfoDTO;
}
