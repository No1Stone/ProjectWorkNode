package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.jpa.entity.Project;
import com.ui.projectworknode.project.jpa.entity.ProjectSong;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectSongRepository extends JpaRepository<ProjectSong, Long> {


}