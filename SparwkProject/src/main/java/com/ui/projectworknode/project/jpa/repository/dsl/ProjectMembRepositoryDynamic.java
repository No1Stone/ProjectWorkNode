package com.ui.projectworknode.project.jpa.repository.dsl;

import com.ui.projectworknode.project.biz.v1.member.dto.ProjectMembRequest;
import com.ui.projectworknode.project.biz.v1.member.dto.ProjectMembResponse;

import java.util.List;

public interface ProjectMembRepositoryDynamic {

    List<ProjectMembResponse> projectMembList(Long projId);
}
