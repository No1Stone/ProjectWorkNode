package com.ui.projectworknode.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SparwkProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SparwkProjectApplication.class, args);
    }

}
