package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.biz.v1.common.dto.*;
import com.ui.projectworknode.project.jpa.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface NativeRepository extends JpaRepository<Project, Long> {

    @Query(nativeQuery = true, value = "select dcode, val from tb_admin_common_detail_code where dcode in :dcodes")
    List<AttrDtlCdResponse> getAttrDtlCdValList(@Param("dcodes") List<String> dcodes);

    @Query(nativeQuery = true, value = "select dcode, role as val from tb_admin_role_detail_code where dcode in :dcodes")
    List<AttrDtlCdResponse> getRoleValList(@Param("dcodes") List<String> dcodes);

    @Query(nativeQuery = true, value = "select dcode, val from tb_admin_song_detail_code where dcode in :dcodes")
    List<AttrDtlCdResponse> getSongCdValList(@Param("dcodes") List<String> dcodes);

    @Query(nativeQuery = true, value = "select language_cd as languageCd, language  from tb_admin_language_code where language_cd in :dcodes")
    List<LanguageCdResponse> getLanguageValList(@Param("dcodes") List<String> dcodes);

    @Query(nativeQuery = true, value = "select val from tb_admin_common_detail_code where dcode = :dcode")
    String getCodeName(@Param("dcode") String dcode);



    @Query(nativeQuery = true, value = "select profile_id as profileId,headline as headline, full_name as fullName, profile_img_url as profileImgUrl, hire_me_yn as hireMeYn from tb_profile where profile_id = :profileId")
    ProfileResponse findProfileInfo(@Param("profileId") Long profileId);

    @Query(nativeQuery = true, value = "select\n" +
            "    tpp.company_profile_id as companyProfileId\n" +
            "   ,profile_company_name as profileCompanyName\n" +
            " from tb_profile_position tpp left join tb_profile_company tpc\n" +
            " on tpp.company_profile_id = tpc.profile_id\n" +
            " where tpp.company_verify_yn ='Y' \n" +
            " and tpp.primary_yn ='Y' \n" +
            " and tpp.profile_id = :profileId limit 1 ")
    CompanyResponse findCompanyInfo(@Param("profileId") Long profileId);


    @Query(nativeQuery = true, value = "select\n" +
            "    tpc.profile_company_name as profileCompanyName\n" +
            "    ,tpc.profile_img_url     as profileImgUrl\n" +
            "    ,tacl.location_cd        as locationCd\n" +
            "    ,tcc.country             as country\n" +
            "    ,tcd.city                as city\n" +
            " from tb_profile_company tpc\n" +
            "    left join tb_account_company_location tacl on tpc.accnt_id = tacl.accnt_id\n" +
            "    left join tb_admin_country_code tcc on tacl.location_cd = tcc.country_cd\n" +
            "    left join tb_account_company_detail tcd on tpc.accnt_id = tcd.accnt_id\n" +
            " where profile_id = :profileId ")
    CompanyResponse findCompanyInfoDetail(@Param("profileId") Long profileId);


    @Query(nativeQuery = true, value = "select              \n" +
            "    tp.profile_id                   as profileId\n" +
            "     ,tp.headline                   as headline\n" +
            "     ,tp.profile_img_url            as profileImgUrl\n" +
            "     ,tp.full_name                  as fullName\n" +
            "     ,tp.bio                        as bio\n" +
            "     ,tp.current_city_country_cd    as currentCityCountryCd\n" +
            "     ,tacc.country                  as currentCityCountryNm\n" +
            "     ,tp.current_city_nm            as currentCityNm\n" +
            "     ,tp.home_town_country_cd       as homeTownCountryCd\n" +
            "     ,tp.hire_me_yn                 as hireMeYn\n" +
            "     ,tpp.company_verify_yn         as companyVerifyYn" +
            "     ,coalesce((select creator_yn from tb_profile_position A where A.profile_id = tpp.profile_id and A.creator_yn is not null order by A.creator_yn desc limit 1),'N')  as createYn\n" +
            "     ,coalesce((select artist_yn from tb_profile_position A where A.profile_id = tpp.profile_id and A.artist_yn is not null order by A.artist_yn desc limit 1),'N')     as artistYn\n" +
            "    ,coalesce((select anr_yn from tb_profile_position A where A.profile_id = tpp.profile_id and A.anr_yn is not null order by A.anr_yn desc limit 1),'N')               as anrYn\n" +
            "     ,tpp.company_profile_id        as companyProfileId\n" +
            "     ,tpp.primary_yn                as primaryYn\n" +
            "     ,tpc.profile_company_name      as profileCompanyName\n" +
            " from tb_profile_position tpp\n" +
            "         left join tb_profile tp on tp.profile_id = tpp.profile_id\n" +
            "         left join tb_profile_company tpc on tpp.company_profile_id = tpc.profile_id\n" +
            "         left join tb_admin_country_code tacc on tacc.country_cd = tp.current_city_country_cd\n" +
            " where tpp.company_verify_yn ='Y'   \n" +
            "  and tpp.profile_id = :profileId  \n" +
            "    and tpp.primary_yn ='Y'        \n" +
            " order by tpp.primary_yn            \n" +
            "limit 1")
    ProfileInfo findProfileBase(@Param("profileId") Long profileId);


    @Query(nativeQuery = true, value = "select\n" +
            "       count(*)\n" +
            " from tb_project_song tps\n" +
            " inner join tb_song ts on tps.song_id = ts.song_id\n" +
            " where proj_id = :projId \n" +
            " and ts.song_status_cd is not null \n" +
            " and ts.song_status_cd != 'ANR000026' ")
    long countProjectPitchedSong(@Param("projId") Long projId);



    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update tb_song set\n" +
            "            song_owner = :songOwner \n" +
            "            where song_id in (\n" +
            "            select\n" +
            "                    song_id\n" +
            "    from tb_project_song\n" +
            "                    where proj_id = :projId \n" +
            "    )")
    void updateSongOwnerChange(@Param("songOwner") Long songOwner, @Param("projId") Long projId);


    @Query(nativeQuery = true, value = "select coalesce(sum(tsc.rate_share),0) from tb_song ts\n" +
            "    inner join tb_song_cowriter tsc on ts.song_id = tsc.song_id\n" +
            "where ts.song_id in (\n" +
            "    select\n" +
            "        song_id\n" +
            "    from tb_project_song\n" +
            "    where proj_id = :projId \n" +
            ")\n" +
            "and tsc.profile_id = :profileId \n" +
            "group by ts.song_id ")
    List<Long> cowriterSplitRateSum(@Param("projId") Long projId, @Param("profileId") Long profileId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from tb_song_cowriter\n" +
            "    where song_id in (\n" +
            "            select\n" +
            "                    song_id\n" +
            "    from tb_project_song\n" +
            "                    where proj_id = :projId \n" +
            "    )\n" +
            "    and profile_id = :profileId ")
    void deleteSongCowriter(@Param("projId") Long projId, @Param("profileId") Long profileId);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from tb_song_credit\n" +
            "    where song_id in (\n" +
            "            select\n" +
            "                    song_id\n" +
            "    from tb_project_song\n" +
            "                    where proj_id = :projId \n" +
            "    )\n" +
            "    and profile_id = :profileId ")
    void deleteSongCredit(@Param("projId") Long projId, @Param("profileId") Long profileId);

}