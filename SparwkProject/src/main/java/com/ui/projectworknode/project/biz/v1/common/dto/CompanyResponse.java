package com.ui.projectworknode.project.biz.v1.common.dto;

public interface CompanyResponse {

    Long getCompanyProfileId();
    String getProfileCompanyName();
    String getProfileImgUrl();
    String getLocationCd();
    String getCountry();
    String getCity();

}
