package com.ui.projectworknode.project.biz.v1.project.dto.project;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileDTO;
import com.ui.projectworknode.project.biz.v1.project.dto.metadata.ProjectMetaResponse;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectResponse {

    private long projId;
    private long projOwner;
    private String ownerName;
    private String fullName;
    private String profileImgUrl;

    private String projIndivCompType;
    private String projIndivCompTypeVal;

    private String projPublYn;
    private String projPwd;
    private String projTitle;
    private String bookmarkYn = "N";
    private List<ProjectMetaResponse> metadatas;
    private String confirmYn;
//    private String rsvpYn;
    private String invtStat;
    private String applyStat;
    private Long compProfileId;
    private String compProfileName;
    private String compProfileImgUrl;
    private String compCountry;
    private String compCity;


    private String completeYn;
    private String projCondCd;
    private String projCondCdVal;
    private String pitchProjTypeCd;
    private String pitchProjTypeCdVal;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projAvalYn;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projInvtTitle;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projInvtDesc;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projWhatFor;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projWorkLocat;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projWorkLocatVal;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projDesc;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projDdlDtStr;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDate projDdlDt;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private long minVal;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private long maxVal;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String recruitYn;

    private String avatarFileUrl;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String leaveYn;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long regUsr;

    private LocalDateTime regDt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long modUsr;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime modDt;

    /* target table company */
    private String inviteCompCd;

    /* target table service */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String serviceCntrCdList;


    /* target table tb_project_metadata */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rolesCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rolesList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String genresCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String genresList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String moodsCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String moodsList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String themesCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String themesList;

    /* target table tb_project_reference_file */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ProjectReferenceFileDTO> files;
}
