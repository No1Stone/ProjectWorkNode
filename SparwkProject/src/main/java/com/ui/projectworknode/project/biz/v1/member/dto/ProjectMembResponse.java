package com.ui.projectworknode.project.biz.v1.member.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ui.projectworknode.project.biz.v1.common.dto.ProfileResponse;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class ProjectMembResponse {

    private Long projId;
    private Long profileId;
    private LocalDateTime patpSdt;
    private LocalDateTime patpEdt;
    private String activeYn;
    private String confirmYn;
    private String applyStat;
    private String invtStat;
    private LocalDateTime invtRegDt;
    private LocalDateTime invtStatChgdt;
    private String quitYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

    private ProfileResponse profile;

    @ApiModelProperty(name = "confirm", notes = "프로젝트 접근이 수락된 멤버")
    private String confirm;
    @ApiModelProperty(name = "rsvpProgress", notes = "(private)초대 받은 RSVP")
    private String rsvpProgress;
    @ApiModelProperty(name = "rsvpApproval", notes = "(private)초대 받은 rsvp가 수락 후 프로젝트에 비밀번호를 입력하지 않은 멤버")
    private String rsvpApproval;
    @ApiModelProperty(name = "notConfirm", notes = "(public) recommended")
    private String notConfirm;
}
