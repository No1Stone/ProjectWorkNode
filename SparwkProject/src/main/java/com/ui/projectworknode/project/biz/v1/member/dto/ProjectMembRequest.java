package com.ui.projectworknode.project.biz.v1.member.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMembRequest {

    @ApiModelProperty(value = "프로젝트고유ID", notes = "프로젝트 고유 ID")
    @Schema(name = "projId", description = "project object")
    private Long projId;

    @ApiModelProperty(value = "멤버list", notes = "profileList" , example = "1|2|3|")
    @Schema(name = "profileIdList", description = "project member profileId")
    private String profileIdList;

}
