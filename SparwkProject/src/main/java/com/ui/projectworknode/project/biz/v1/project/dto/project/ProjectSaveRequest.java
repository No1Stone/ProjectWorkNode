package com.ui.projectworknode.project.biz.v1.project.dto.project;

import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileSaveRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectSaveRequest {

    @ApiModelProperty(name = "projId", example = "1", notes = "project Id", value = "projSeq", required = true)
    private Long projId;
    @ApiModelProperty(name = "projOwner", example = "10000000198", notes = "project Owner",value = "projOwnerId", required = true)
    private Long projOwner;

    @ApiModelProperty(name = "genderCd", example = "GND000071", notes = "gender type", value = "genderCd")
    private String genderCd;

    @ApiModelProperty(name = "projIndivCompType", example = "IND000001", value = "projIndivCompType")
    private String projIndivCompType;
    @ApiModelProperty(name = "compProfileId", example = "10000000251", value = "compProfileId")
    private Long compProfileId = 0L;

    @ApiModelProperty(name = "projPublYn", example = "N", value = "projPublYn")
    private String projPublYn;
    @ApiModelProperty(name = "projPwd", example = "1234", value = "projPwd")
    private String projPwd;

    @ApiModelProperty(name = "projInvtTitle", example = "sample title", value = "projInvtTitle")
    private String projInvtTitle;
    @ApiModelProperty(name = "projInvtDesc", example = "sample invt Desc", value = "projInvtDesc")
    private String projInvtDesc;
    @ApiModelProperty(name = "projWhatFor", example = "project what for", value = "projWhatFor")
    private String projWhatFor;

    @ApiModelProperty(name = "pitchProjTypeCd", example = "PJT000001", value = "pitchProjTypeCd")
    private String pitchProjTypeCd;

    @ApiModelProperty(name = "projWorkLocat", example = "PWL000001", value = "projWorkLocat")
    private String projWorkLocat;

    @ApiModelProperty(name = "projTitle", example = "project Title", value = "projTitle")
    private String projTitle;
    @ApiModelProperty(name = "projDesc", example = "project Desc", value = "projDesc")
    private String projDesc;
    @ApiModelProperty(name = "projDdlDtStr", example = "20220101", value = "projDdlDtStr")
    private String projDdlDtStr;

    @ApiModelProperty(name = "projCondCd", example = "PJC000001", value = "projCondCd")
    private String projCondCd;

    @ApiModelProperty(name = "minVal", example = "0", value = "minVal")
    private Long minVal = 0L;
    @ApiModelProperty(name = "maxVal", example = "0", value = "maxVal")
    private Long maxVal = 0L;
    @ApiModelProperty(name = "avatarFileUrl", example = "https://examewonseokbucket.s3-ap-northeast-2.amazonaws.com/9000386.png", value = "avatarFileUrl")
    private String avatarFileUrl;

    @ApiModelProperty(name = "serviceCntrCdList", example = "CNT000060,CNT000061,CNT000064,CNT000069", value = "serviceCntrCdList")
    private String serviceCntrCdList;
    @ApiModelProperty(name = "metadataList", example = "SGT000087,SGE000006,SGG000039,SGP000008,DEX000017,SGT001265,SGS000235,SGP000002,SGG000029", value = "metadataList")
    private String metadataList;
    /* target table tb_project_reference_file */
    private List<ProjectReferenceFileSaveRequest> files;
}
