package com.ui.projectworknode.project.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectMetadataId implements Serializable {
    private long projId;
    private String attrTypeCd;
    private String attrDtlCd;
}
