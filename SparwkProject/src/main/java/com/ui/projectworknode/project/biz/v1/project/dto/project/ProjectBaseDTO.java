package com.ui.projectworknode.project.biz.v1.project.dto.project;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileDTO;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectBaseDTO {

    private Long projId;

    private Long projOwner;
    private String projAvalYn;
    private String genderCd;


    private Long compProfileId;
    private String projIndivCompType;
    private String projIndivCompTypeVal;


    private String projPublYn;
    private String projPwd;
    private String projInvtTitle;
    private String projInvtDesc;
    private String projWhatFor;

    private String pitchProjTypeCd;
    private String pitchProjTypeCdVal;

    private String projWorkLocat;
    private String projWorkLocatVal;

    private String projTitle;
    private String projDesc;
    private String projDdlDtStr;
    private LocalDate projDdlDt;

    private String projCondCd;
    private String projCondCdVal;

    private Long minVal;
    private Long maxVal;
    private String recruitYn;
    private String avatarFileUrl;
    private String leaveYn;
    private String completeYn;

    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

    /* target table company */
    private String inviteCompCd;

    /* target table service */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String serviceCntrCdList;


    /* target table tb_project_metadata */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rolesCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rolesList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String genresCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String genresList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String moodsCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String moodsList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String themesCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String themesList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String languagesCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String languageList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String temposCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String temposList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String instrumentsCd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String instrumentsList;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rsvpYn;
    private String bookmarkYn;


    /* target table tb_project_reference_file */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ProjectReferenceFileDTO> files;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int startRow;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int endRow;
}
