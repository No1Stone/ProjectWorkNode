package com.ui.projectworknode.project.jpa.repository.dsl;

import com.ui.projectworknode.project.biz.v1.project.dto.ProjectBookmarkRequest;

public interface ProjectBookmarkRepositoryDynamic {

    long projectBookmarkDynamicUpdate(ProjectBookmarkRequest projectBookmarkRequest);
}
