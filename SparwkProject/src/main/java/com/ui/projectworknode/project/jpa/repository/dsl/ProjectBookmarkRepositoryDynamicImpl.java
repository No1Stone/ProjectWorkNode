package com.ui.projectworknode.project.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.ui.projectworknode.project.biz.v1.project.dto.ProjectBookmarkRequest;
import com.ui.projectworknode.project.jpa.entity.QProjectBookmark;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Aspect
public class ProjectBookmarkRepositoryDynamicImpl implements ProjectBookmarkRepositoryDynamic {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProjectBookmarkRepositoryDynamicImpl.class);

    private ProjectBookmarkRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public long projectBookmarkDynamicUpdate(ProjectBookmarkRequest req) {
        logger.info("### projectBookmarkDynamicUpdate ### {} ", req);
        QProjectBookmark qProjectBookmark = QProjectBookmark.projectBookmark;

        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProjectBookmark);

        jpaUpdateClause.set(qProjectBookmark.bookmarkYn, req.getBookmarkYn());
        jpaUpdateClause.set(qProjectBookmark.modUsr, req.getProfileId());
        jpaUpdateClause.set(qProjectBookmark.modDt, LocalDateTime.now());

        jpaUpdateClause.where(
                qProjectBookmark.projId.eq(req.getProjId())
                .and(qProjectBookmark.profileId.eq(req.getProfileId())));
        long result = jpaUpdateClause.execute();
        //TODO : result 값에 따라 res 결과 반환
        em.flush();
        em.clear();
        return result;
    }

}
