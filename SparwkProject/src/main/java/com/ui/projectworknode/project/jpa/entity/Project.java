package com.ui.projectworknode.project.jpa.entity;

import com.ui.projectworknode.project.constants.CommonCodeConst;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project")
public class Project  {
    @Id
//    @GeneratedValue(generator = "tb_project_seq")
    @Column(name = "proj_id", nullable = false)
    private Long projId;
    @Column(name = "proj_owner", nullable = false)
    private Long projOwner;
    @Column(name = "proj_aval_yn", nullable = true)
    private String projAvalYn;
    @Column(name = "gender_cd", nullable = true)
    private String genderCd;
    @Column(name = "proj_indiv_comp_type", nullable = true)
    private String projIndivCompType;
    @Column(name = "proj_publ_yn", nullable = true)
    private String projPublYn;
    @Column(name = "proj_pwd", nullable = true)
    private String projPwd;
    @Column(name = "proj_invt_title", nullable = true)
    private String projInvtTitle;
    @Column(name = "proj_invt_desc", nullable = true)
    private String projInvtDesc;
    @Column(name = "proj_what_for", nullable = true)
    private String projWhatFor;
    @Column(name = "pitch_proj_type_cd", nullable = true)
    private String pitchProjTypeCd;
    @Column(name = "proj_work_locat", nullable = true)
    private String projWorkLocat;
    @Column(name = "proj_title", nullable = true)
    private String projTitle;
    @Column(name = "proj_desc", nullable = true)
    private String projDesc;
    @Column(name = "proj_ddl_dt", nullable = true)
    private LocalDate projDdlDt;
    @Column(name = "proj_cond_cd", nullable = true)
    private String projCondCd;
    @Column(name = "min_val", nullable = true)
    private Long minVal;
    @Column(name = "max_val", nullable = true)
    private Long maxVal;
    @Column(name = "recruit_yn", nullable = true)
    private String recruitYn;
    @Column(name = "avatar_file_url", nullable = true)
    private String avatarFileUrl;
    @Column(name = "leave_yn", nullable = true)
    private String leaveYn;
    @Column(name = "complete_yn", nullable = true)
    private String completeYn;
    @Column(name = "comp_profile_id", nullable = true)
    private Long compProfileId;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    Project(
            Long projId,
            Long projOwner,
            String projAvalYn,
            String projIndivCompType,
            String projPublYn,
            String projPwd,
            String projInvtTitle,
            String projInvtDesc,
            String projWhatFor,
            String pitchProjTypeCd,
            String projWorkLocat,
            String projTitle,
            String projDesc,
            LocalDate projDdlDt,
            String projCondCd,
            Long minVal,
            Long maxVal,
            String recruitYn,
            String avatarFileUrl,
            String leaveYn,
            String completeYn,
            Long compProfileId,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.projId = projId;
        this.projOwner = projOwner;
        this.projAvalYn = projAvalYn;
        this.projIndivCompType = projIndivCompType != null ? projIndivCompType : CommonCodeConst.PROJ_INDIV_COMP_TYPE.INDIVIDUAL;
        this.projPublYn = projPublYn != null ? projPublYn : CommonCodeConst.YN.Y;
        this.projPwd = projPwd != null ? projPwd : "";
        this.projInvtTitle = projInvtTitle != null ? projInvtTitle : "";
        this.projInvtDesc = projInvtDesc != null ? projInvtDesc : "";
        this.projWhatFor = projWhatFor != null ? projWhatFor : "";
        this.pitchProjTypeCd = pitchProjTypeCd != null ? pitchProjTypeCd.trim() : "";
        this.projWorkLocat = projWorkLocat != null ? projWorkLocat.trim() : CommonCodeConst.PROJ_WORK_LOCAT.BY_ONLINE;
        this.projTitle = projTitle != null ? projTitle : "";
        this.projDesc = projDesc != null ? projDesc : "";
        this.projDdlDt = projDdlDt != null ? projDdlDt : LocalDate.now();
        this.projCondCd = projCondCd != null ? projCondCd.trim() : CommonCodeConst.PROJ_COND_CD.TERMS;
        this.minVal = minVal != 0 ? minVal : 0;
        this.maxVal = maxVal != 0 ? maxVal : 0;
        this.recruitYn = recruitYn;
        this.avatarFileUrl = avatarFileUrl != null ? avatarFileUrl : "";
        this.leaveYn = leaveYn;
        this.completeYn = completeYn;
        this.compProfileId = compProfileId;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectMetadata> projectMetadata = new ArrayList<>();
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectReferenceFile> projectReferenceFiles = new ArrayList<>();
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private ProjectInviteCompany projectInviteCompany;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectServiceCountry> projectServiceCountries = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectSong> projectSongs = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private List<ProjectSongCowriter> projectSongCowriters = new ArrayList<>();

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="project")
//    private ProjectMemb projectMemb;

    //
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProjectMemb.class)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
//    private ProjectMemb projectMemb;
//
//    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProjectSong.class)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id")
//    private ProjectSong projectSong;

}
