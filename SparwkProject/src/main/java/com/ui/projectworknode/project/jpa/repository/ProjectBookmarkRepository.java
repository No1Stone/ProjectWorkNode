package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.biz.v1.project.dto.ProjectBookmarkRequest;
import com.ui.projectworknode.project.jpa.entity.ProjectBookmark;
import com.ui.projectworknode.project.jpa.repository.dsl.ProjectBookmarkRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectBookmarkRepository extends JpaRepository<ProjectBookmark, Long>, ProjectBookmarkRepositoryDynamic {
    ProjectBookmark findProjectBookmarkByProjIdAndProfileId(long projId, long profileId);
}