package com.ui.projectworknode.project.biz.v1.common.dto;

public interface ProfileResponse {

    Long getProjId();
    String getFullName();
    String getProfileImgUrl();
    String getHeadline();
    String getHireMeYn();
}
