package com.ui.projectworknode.project.jpa.entity;


import com.ui.projectworknode.project.jpa.entity.id.ProjectMetadataId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@IdClass(ProjectMetadataId.class)
@Table(name = "tb_project_metadata")
public class ProjectMetadata  {

    @Id
    @Column(name = "proj_id", nullable = false)
    private long projId;
    @Id
    @Column(name = "attr_type_cd", nullable = false)
    private String attrTypeCd;
    @Id
    @Column(name = "attr_dtl_cd", nullable = false)
    private String attrDtlCd;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    ProjectMetadata(
            Long projId,
            String attrTypeCd,
            String attrDtlCd,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
            this.projId =   projId;
            this.attrTypeCd = attrTypeCd.trim();
            this.attrDtlCd   = attrDtlCd.trim();
            this.regUsr                 =       regUsr;
            this.regDt                  =       regDt;
            this.modUsr                 =       modUsr;
            this.modDt                  =       modDt;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;
}
