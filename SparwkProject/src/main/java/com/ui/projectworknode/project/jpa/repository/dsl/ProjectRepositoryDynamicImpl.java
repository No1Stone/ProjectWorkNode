package com.ui.projectworknode.project.jpa.repository.dsl;

import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectBaseDTO;
import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectFilter;
import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectRequest;
import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectResponse;
import com.ui.projectworknode.project.constants.CommonCodeConst;
import com.ui.projectworknode.project.jpa.entity.QProject;
import com.ui.projectworknode.project.jpa.entity.QProjectBookmark;
import com.ui.projectworknode.project.jpa.entity.QProjectMemb;
import com.ui.projectworknode.project.jpa.entity.QProjectMetadata;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Aspect
public class ProjectRepositoryDynamicImpl implements ProjectRepositoryDynamic{
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProjectRepositoryDynamicImpl.class);

    private QProject qProject = QProject.project;
    private QProjectBookmark qProjectBookmark = QProjectBookmark.projectBookmark;
    private QProjectMemb qProjectMemb = QProjectMemb.projectMemb;
    private QProjectMetadata qProjectMetadata = QProjectMetadata.projectMetadata;

    private ProjectRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public long updateprojectOwnerChange(ProjectBaseDTO dto) {
        logger.info("### ProjectRepositoryDynamicUpdate ### {} ", dto);

        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProject);
        // change Owner
        if (dto.getProjOwner() != null && dto.getProjOwner() != 0) {
            jpaUpdateClause.set(qProject.projOwner, dto.getProjOwner());
        }

        jpaUpdateClause.set(qProject.modUsr, dto.getProjOwner());
        jpaUpdateClause.set(qProject.modDt, LocalDateTime.now());

        jpaUpdateClause.where(qProject.projId.eq(dto.getProjId()));

        long result = 0;
        result = jpaUpdateClause.execute();
        //TODO : result 값에 따라 res 결과 반환
        em.flush();
        em.clear();

        return result;
    }

    @Override
    public long projectRepositoryDynamicUpdate(ProjectBaseDTO dto) {
        logger.info("### ProjectRepositoryDynamicUpdate ### {} ", dto);

        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProject);

        // recruting update
        if (!StringUtils.isEmpty(dto.getRecruitYn())) {
            jpaUpdateClause.set(qProject.recruitYn, dto.getRecruitYn());
        }

        // RSVP Description update
        if (!StringUtils.isEmpty(dto.getProjWhatFor())) {
            jpaUpdateClause.set(qProject.projWhatFor, dto.getProjWhatFor());
        }

        // Location update
        if (!StringUtils.isEmpty(dto.getProjWorkLocat())) {
            jpaUpdateClause.set(qProject.projWorkLocat, dto.getProjWorkLocat());
        }

        // Budgeted or Terms update
        if (!StringUtils.isEmpty(dto.getProjCondCd())) {
            jpaUpdateClause.set(qProject.projCondCd, dto.getProjCondCd());
        }
        jpaUpdateClause.set(qProject.minVal, dto.getMinVal());
        jpaUpdateClause.set(qProject.maxVal, dto.getMaxVal());

        // change Owner
        if (dto.getProjOwner() != null && dto.getProjOwner() != 0) {
            jpaUpdateClause.set(qProject.projOwner, dto.getProjOwner());
        }

        // Leave yn
        if (!StringUtils.isEmpty(dto.getLeaveYn())) {
            jpaUpdateClause.set(qProject.leaveYn, dto.getLeaveYn());
        }

        jpaUpdateClause.set(qProject.modUsr, dto.getProjOwner());
        jpaUpdateClause.set(qProject.modDt, LocalDateTime.now());

        jpaUpdateClause.where(qProject.projId.eq(dto.getProjId()));

        long result = 0;
        result = jpaUpdateClause.execute();
        //TODO : result 값에 따라 res 결과 반환
        em.flush();
        em.clear();

        return result;
    }


    @Override
    public List<ProjectResponse> projectListDynamic(ProjectRequest req, ProjectFilter projectFilter) {

        List<ProjectResponse> result = null;

        OrderSpecifier<?> order = getSortedColumn(Order.DESC, qProject.regDt, "regDt");
        if(StringUtils.equals(projectFilter.getSort(), "projTitle")) {
            Order direction = StringUtils.equals(projectFilter.getSortDirection(), "ASC") ? Order.ASC : Order.DESC;
            order = getSortedColumn(direction, qProject.projTitle, "projTitle");
        }

        try {
            result = queryFactory.select(Projections.fields(ProjectResponse.class,
                                    qProject.projId, qProject.projOwner, qProject.projTitle, qProject.projPublYn,
                                    qProject.projPwd, qProject.regDt, qProject.avatarFileUrl,
                                    qProject.projIndivCompType, qProject.projDdlDt, qProject.compProfileId,
                                    qProject.pitchProjTypeCd, qProject.projCondCd,
                                    qProject.minVal, qProject.maxVal,
                                    qProjectBookmark.bookmarkYn.coalesce(CommonCodeConst.YN.N).as("bookmarkYn"),
                                    ExpressionUtils.as(
                                        JPAExpressions.select(qProjectMemb.confirmYn)
                                                .from(qProjectMemb)
                                                .where(qProjectMemb.projId.eq(qProject.projId)
                                                        .and(qProjectMemb.profileId.eq(req.getProfileId()))
                                                )
                                                .limit(1)
                                        ,"confirmYn"
                                    ),
                                    ExpressionUtils.as(
                                        JPAExpressions.select(qProjectMemb.invtStat)
                                                .from(qProjectMemb)
                                                .where(qProjectMemb.projId.eq(qProject.projId)
                                                        .and(qProjectMemb.profileId.eq(req.getProfileId()))
                                                )
                                                .limit(1)
                                        ,"invtStat"
                                    ),
                                    ExpressionUtils.as(
                                    JPAExpressions.select(qProjectMemb.applyStat)
                                            .from(qProjectMemb)
                                            .where(qProjectMemb.projId.eq(qProject.projId)
                                                    .and(qProjectMemb.profileId.eq(req.getProfileId()))
                                            )
                                            .limit(1)
                                    ,"applyStat"
                                )
                            )
                    )
                    .from(qProject)
                    .leftJoin(qProjectBookmark).on(qProjectBookmark.projId.eq(qProject.projId).and(qProjectBookmark.profileId.eq(req.getProfileId())))
//                    .leftJoin(qProjectMemb).on(qProjectMemb.projId.eq(qProject.projId).and(qProjectMemb.profileId.eq(req.getProfileId())))
                    .where(qProject.projId.in(JPAExpressions.select(qProjectMemb.projId)
                                    .from(qProjectMemb)
                                    .where(qProjectMemb.profileId.eq(req.getProfileId()))
                        )
                        ,likeProjTitle(projectFilter.getProjTitle())
                        ,eqPitchProjTypeCd(projectFilter.getPitchProjTypeCd())
                        ,eqGenderCd(projectFilter.getGenderCd())
                        ,inMetadata(projectFilter.getAttrDtlCdList())
                    )
                    .orderBy(order)
                    .offset(projectFilter.getOffset())
                    .limit(projectFilter.getLimit())
                    .fetch();

        } catch (Exception e){
            logger.error("####### JPA LIST FETCH ERROR ####### {} ",e.getCause());
        }
        return result;
    }


    private OrderSpecifier<?> getSortedColumn(Order order, Path<?> parent, String fieldName) {
        Path<Object> fieldPath = Expressions.path(Object.class, parent, fieldName);
        return new OrderSpecifier(order, fieldPath);
    }
    private Sort sortByRegDtDesc() {
        return Sort.by(Sort.Direction.DESC, "regDt");
    }
    private Sort sortByProjTitleDesc() {
        return Sort.by(Sort.Direction.DESC, "projTitle");
    }
    private Sort sortByProjTitleAsc() {
        return Sort.by(Sort.Direction.ASC, "regDt");
    }


    // filter
    // project -> pitch_proj_type_cd, projecttitle, gender..., sorting
    private BooleanExpression likeProjTitle(String projTitle){
        if (StringUtils.isEmpty(projTitle)) {
            return null;
        }
        return qProject.projTitle.contains(projTitle);
    }


    private BooleanExpression eqPitchProjTypeCd(String pitchProjTypeCd){
        if (StringUtils.isEmpty(pitchProjTypeCd)) {
            return null;
        }
        return qProject.pitchProjTypeCd.contains(pitchProjTypeCd);
    }

    private BooleanExpression eqGenderCd(String genderCd){
        if (StringUtils.isEmpty(genderCd)) {
            return null;
        }
        return qProject.genderCd.contains(genderCd);
    }

    private BooleanExpression inMetadata(String attrDtlCdList){
        if (StringUtils.isEmpty(attrDtlCdList)) {
            return null;
        }
        List<String> attrDtlCds = Arrays.stream(attrDtlCdList.split(","))
                .map(e -> e.trim()).collect(Collectors.toList());
        return qProject.projId.in(JPAExpressions
                .select(qProjectMetadata.projId)
                .from(qProjectMetadata)
                .where(qProjectMetadata.attrDtlCd.in(attrDtlCds))
        );
    }
}
