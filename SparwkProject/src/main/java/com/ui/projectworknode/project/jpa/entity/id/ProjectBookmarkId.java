package com.ui.projectworknode.project.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectBookmarkId implements Serializable {
    private long profileId;
    private long projId;
    private String bookmarkYn;
}
