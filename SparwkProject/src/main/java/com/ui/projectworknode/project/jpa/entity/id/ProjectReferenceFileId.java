package com.ui.projectworknode.project.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectReferenceFileId implements Serializable {
    private Long projId;
    private Long fileFefSeq;
}
