package com.ui.projectworknode.project.jpa.entity;

import com.ui.projectworknode.project.jpa.entity.id.ProjectInviteCompanyId;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@IdClass(ProjectInviteCompanyId.class)
@Table(name = "tb_project_invite_company")
public class ProjectInviteCompany  {

    @Id
    @Column(name = "proj_id", nullable = false)
    private long projId;
    @Id
    @Column(name = "comp_profile_id", nullable = false)
    private Long compProfileId;

    @Column(name = "reg_usr")
    @CreatedBy
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    ProjectInviteCompany(
            long projId,
            Long compProfileId,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
            this.projId =   projId;
            this.compProfileId = compProfileId;
            this.regUsr                 =       regUsr;
            this.regDt                  =       regDt;
            this.modUsr                 =       modUsr;
            this.modDt                  =       modDt;
    }
}
