package com.ui.projectworknode.project.jpa.repository.dsl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.ui.projectworknode.project.biz.v1.project.dto.metadata.ProjectMetaResponse;
import com.ui.projectworknode.project.jpa.entity.ProjectMetadata;
import com.ui.projectworknode.project.jpa.entity.QProject;
import com.ui.projectworknode.project.jpa.entity.QProjectMetadata;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

@Aspect
public class ProjectMetadataRepositoryDynamicImpl implements ProjectMetadataRepositoryDynamic{
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private QProject qProject = QProject.project;
    private final Logger logger = LoggerFactory.getLogger(ProjectMetadataRepositoryDynamicImpl.class);

    private ProjectMetadataRepositoryDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public long projectMetadataDynamicDelete(ProjectMetadata dto) {
        logger.info("### projectMetadataDynamicDelete ### {} ", dto);
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProject);
        jpaUpdateClause.where(qProject.projId.eq(dto.getProjId()));
        long result = jpaUpdateClause.execute();
        //TODO : result 값에 따라 res 결과 반환
        em.flush();
        em.clear();
        return result;
    }

    @Override
    public List<ProjectMetaResponse> projectMetadataDetailDynamic(Long projId) {
        return null;
    }

    @Override
    public List<ProjectMetaResponse> projectMetadataListDynamic(Long projId) {
        logger.info("<<<<< projectMetadataListDynamic {]", projId);
        QProjectMetadata qProjectMetadata = QProjectMetadata.projectMetadata;

        List<ProjectMetaResponse> result = queryFactory.select(Projections.fields(ProjectMetaResponse.class,
                qProjectMetadata.attrTypeCd
                        ,qProjectMetadata.attrDtlCd
                )
        ).from(qProjectMetadata)
        .where(qProjectMetadata.projId.eq(projId))
        .fetch();

        return result;
    }
}
