package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.jpa.entity.ProjectServiceCountry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectServiceCountryRepository extends JpaRepository<ProjectServiceCountry, Long>{
}