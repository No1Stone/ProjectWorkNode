package com.ui.projectworknode.project.biz.v1.project.service;

import com.ui.projectworknode.project.biz.v1.common.CommonCodeApplication;
import com.ui.projectworknode.project.biz.v1.common.dto.AttrDtlCdResponse;
import com.ui.projectworknode.project.biz.v1.common.dto.CompanyResponse;
import com.ui.projectworknode.project.biz.v1.common.dto.LanguageCdResponse;
import com.ui.projectworknode.project.biz.v1.common.dto.ProfileResponse;
import com.ui.projectworknode.project.biz.v1.project.dto.ProjectBookmarkRequest;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileDTO;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileModifyRequest;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFilesModifyRequest;
import com.ui.projectworknode.project.biz.v1.project.dto.metadata.ProjectMetaDataDTO;
import com.ui.projectworknode.project.biz.v1.project.dto.metadata.ProjectMetaResponse;
import com.ui.projectworknode.project.biz.v1.project.dto.project.*;
import com.ui.projectworknode.project.config.common.Response;
import com.ui.projectworknode.project.config.filter.token.UserInfoDTO;
import com.ui.projectworknode.project.constants.CommonCodeConst;
import com.ui.projectworknode.project.constants.ProjectMembConst;
import com.ui.projectworknode.project.constants.ResultCodeConst;
import com.ui.projectworknode.project.jpa.entity.*;
import com.ui.projectworknode.project.jpa.repository.*;
import com.ui.projectworknode.project.util.TokenUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.TransactionalException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProjectService {
    private final Logger logger = LoggerFactory.getLogger(ProjectService.class);

    private final ModelMapper modelMapper;

    private final ProjectRepository projectRepository;
    private final ProjectMetadataRepository projectMetadataRepository;
    private final ProjectReferenceFileRepository projectReferenceFileRepository;
    private final ProjectInviteCompanyRepository projectInviteCompanyRepository;
    private final ProjectServiceCountryRepository projectServiceCountryRepository;
    private final ProjectMembRepository projectMembRepository;
    private final ProjectBookmarkRepository projectBookmarkRepository;

    // common
    private final CommonCodeApplication commonCodeApplication;

    private final MessageSourceAccessor messageSourceAccessor;
    private final HttpServletRequest request;


    public Response<Long> initProjSeq() {
        logger.info("<<<<< initProjSeq ");
        Response<Long> res = new Response<>();
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(projectRepository.newProjectSeq());
        return res;
    }

    /**
     * Create Project
     * @param dto
     * @return
     */
    @Transactional
    public Response<Long> projectSaveService(ProjectSaveRequest dto) {

        //TODO: profileId valid
        //TODO: commmon tb_admin_song_detail_code,tb_admin_common_detail_code hit add+1

        Response<Long> res = new Response<>();

        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);
        if(dto == null){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        // projId
        if(dto.getProjId() == null || dto.getProjId() == 0L) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        // projOwner
        if(dto.getProjOwner() == null || dto.getProjOwner() == 0L) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        // projOwner != login user
        if(!userInfoDTO.getLastUseProfileId().equals(dto.getProjOwner())) {
            res.setResultCd(ResultCodeConst.NOT_MATCH_PROJECT_OWNER.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        // dto cut

        ProjectDTO projectDTO = modelMapper.map(dto, ProjectDTO.class);
        // proj_ddl_dt
        if(!StringUtils.isEmpty(dto.getProjDdlDtStr())) {
            projectDTO.setProjDdlDt(LocalDate.parse(dto.getProjDdlDtStr(), DateTimeFormatter.ofPattern("yyyyMMdd")));
        } else {
            // default add 3 months
            projectDTO.setProjDdlDt(LocalDate.now().plusMonths(3));
        }
        projectDTO.setProjAvalYn(CommonCodeConst.YN.Y);
        projectDTO.setRecruitYn(CommonCodeConst.YN.Y);
        projectDTO.setLeaveYn(CommonCodeConst.YN.N);
        projectDTO.setCompleteYn(CommonCodeConst.YN.N);
        projectDTO.setRegUsr(projectDTO.getProjOwner());
        projectDTO.setRegDt(LocalDateTime.now());
        projectDTO.setModUsr(projectDTO.getProjOwner());
        projectDTO.setModDt(LocalDateTime.now());
        projectDTO.setCompProfileId(
                dto.getCompProfileId() == null || dto.getCompProfileId() == 0L ? 0L : dto.getCompProfileId());

        Project entity = modelMapper.map(projectDTO, Project.class);

        try {
            projectRepository.save(entity);

            // 프로젝트 owner save
            ProjectMemb projectMemb = ProjectMemb.builder()
                    .profileId(projectDTO.getProjOwner())
                    .projId(projectDTO.getProjId())
                    .activeYn(CommonCodeConst.YN.Y)
                    .confirmYn(CommonCodeConst.YN.Y)
                    .invtStat(ProjectMembConst.INVT_STAT.APPROVAL.getCode())
//                    .applyStat(ProjectMembConst.APPLY_STAT.APPLY.getCode())
                    .patpSdt(LocalDateTime.now())
                    .banYn(CommonCodeConst.YN.N)
                    .quitYn(CommonCodeConst.YN.N)
                    .regDt(LocalDateTime.now())
                    .regUsr(projectDTO.getProjOwner())
                    .modDt(LocalDateTime.now())
                    .modUsr(projectDTO.getProjOwner())
                    .build();
            projectMembRepository.save(projectMemb);
            //END : 프로젝트 owner save

            // Then project individual type is COMPANY
            if(StringUtils.equals(dto.getProjIndivCompType().toUpperCase().trim(), CommonCodeConst.PROJ_INDIV_COMP_TYPE.COMPANY)) {
                if(projectDTO.getCompProfileId() == null) {
                    res.setResultCd(ResultCodeConst.NOT_VALID_COMPANY_CD.getCode());
                    res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                    return res;
                }
                ProjectInviteCompany company = ProjectInviteCompany.builder()
                        .projId(projectDTO.getProjId())
                        .compProfileId(projectDTO.getCompProfileId())
                        .regUsr(projectDTO.getProjOwner())
                        .regDt(LocalDateTime.now())
                        .modUsr(projectDTO.getProjOwner())
                        .modDt(LocalDateTime.now())
                        .build();
                projectInviteCompanyRepository.save(company);
            }

            // save Meatadata
            if(!StringUtils.isEmpty(dto.getMetadataList())) {
                // save
                Arrays.stream(dto.getMetadataList().split(",")).forEach(e -> {
                    ProjectMetadata vo = ProjectMetadata.builder()
                            .projId(projectDTO.getProjId())
                            .attrTypeCd(e.substring(0,3))
                            .attrDtlCd(e)
                            .regUsr(projectDTO.getProjOwner())
                            .regDt(LocalDateTime.now())
                            .modUsr(projectDTO.getProjOwner())
                            .modDt(LocalDateTime.now())
                            .build();
                    projectMetadataRepository.save(vo);
                });
            }

            // Global Service Area
            if(!StringUtils.isEmpty(dto.getServiceCntrCdList())) {
                Arrays.stream(dto.getServiceCntrCdList().split(",")).forEach(serviceCd -> {
                    ProjectServiceCountry vo = ProjectServiceCountry.builder()
                            .projId(projectDTO.getProjId())
                            .serviceCntrCd(serviceCd)
                            .regUsr(projectDTO.getProjOwner())
                            .regDt(LocalDateTime.now())
                            .modUsr(projectDTO.getProjOwner())
                            .modDt(LocalDateTime.now())
                            .build();
                    projectServiceCountryRepository.save(vo);
                });
            }

            // Files Update
            dto.getFiles().stream().forEach(fileObj -> {
                ProjectReferenceFile vo = ProjectReferenceFile.builder()
                        .projId(projectDTO.getProjId())
                        .uploadTypeCd(fileObj.getUploadTypeCd())
                        .fileName(fileObj.getFileName())
                        .filePath(fileObj.getFilePath())
                        .fileSize(fileObj.getFileSize())
                        .refUrl(fileObj.getRefUrl())
                        .regUsr(projectDTO.getProjOwner())
                        .regDt(LocalDateTime.now())
                        .modUsr(projectDTO.getProjOwner())
                        .modDt(LocalDateTime.now())
                        .build();
                projectReferenceFileRepository.save(vo);
            });


            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            res.setResult(projectDTO.getProjId());

        } catch (Exception e){
            logger.error("ERROR >>>>> {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        return res;
    }

    /**
     * select project list of project owner
     * @param profileId
     * @param projectFilter
     * @return
     */
    public Response<List<ProjectResponse>> getProjectList(Long profileId, ProjectFilter projectFilter) {
        logger.info("##### getProjectList #####");
        Response<List<ProjectResponse>> res = new Response<>();
        ProjectRequest req = ProjectRequest.builder()
                .profileId(profileId)
                .build();

        List<ProjectResponse> result = projectRepository.projectListDynamic(req, projectFilter);

        result.stream().forEach(e -> {
            // profile name, img setting
            ProfileResponse profile = commonCodeApplication.findViewUserInfo(e.getProjOwner());
            if(profile != null) {
                e.setFullName(profile.getFullName());
                e.setProfileImgUrl(profile.getProfileImgUrl());
            }
            // company profile
            CompanyResponse company = commonCodeApplication.findCompanyInfoDetail(e.getCompProfileId());
            if(company != null) {
                e.setCompProfileName(company.getProfileCompanyName());
                e.setCompProfileImgUrl(company.getProfileImgUrl());
                e.setCompCountry(company.getCountry());
                e.setCompCity(company.getCity());
            }

            // name setting
            e.setProjCondCdVal(commonCodeApplication.getCodeName(e.getProjCondCd()));
            e.setProjWorkLocatVal(commonCodeApplication.getCodeName(e.getProjWorkLocat()));
            e.setPitchProjTypeCdVal(commonCodeApplication.getCodeName(e.getPitchProjTypeCd()));
            e.setProjIndivCompTypeVal(commonCodeApplication.getCodeName(e.getProjIndivCompType()));

            ProjectCompleteYnResponse completeYn = projectRepository.findProjIdAndSongCntAndAvailSongCntByProjId(e.getProjId());
            e.setCompleteYn(
                    completeYn != null ? completeYn.getCompleteYn() : CommonCodeConst.YN.N
                    );


            List<ProjectMetaDataDTO> metadataList = projectMetadataRepository.findProjectMetadataByProjId(e.getProjId()).stream()
                    .map(f -> modelMapper.map(f, ProjectMetaDataDTO.class))
                    .collect(Collectors.toList());
            metadataList = this.metadataSetting(metadataList);

//            List<ProjectMetaResponse> metadataList = projectMetadataRepository.projectMetadataListDynamic(e.getProjId());
//            List<String> dcodes = metadataList.stream().map(f -> f.getAttrDtlCd()).collect(Collectors.toList());
//            List<AttrDtlCdResponse> meta = commonCodeApplication.getRoleValList(dcodes);
//            metadataList.stream().forEach(f -> meta.stream().forEach(g -> {
//                if(StringUtils.equals(f.getAttrDtlCd(), g.getDcode())) {
//                    f.setAttrDtlCdVal(g.getVal());
//                }
//            }));

            e.setMetadatas(metadataList.stream().map(f -> modelMapper.map(f, ProjectMetaResponse.class)).collect(Collectors.toList()));
        });

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }

    /**
     * select project one of project projId
     * @param projId
     * @return
     */
    public Response<ProjectBaseDTO> getProjectDetail(Long projId, Long profileId) {
        logger.info("##### getProjectList #####");
        Response<ProjectBaseDTO> res = new Response<>();

        ProjectMemb isProjectMember = projectMembRepository.findProjectMembByProjIdAndProfileId(projId, profileId);
        if(isProjectMember == null){
            res.setResultCd(ResultCodeConst.NOT_EXIST_PROFILE_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        Project projectResult = projectRepository.findProjectByProjId(projId);
        if(projectResult == null) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        ProjectBaseDTO result = modelMapper.map(projectResult, ProjectBaseDTO.class);

        //bookmark
        ProjectBookmark projectBookmark = projectBookmarkRepository.findProjectBookmarkByProjIdAndProfileId(projId, profileId);
        result.setBookmarkYn(projectBookmark != null ? projectBookmark.getBookmarkYn() : CommonCodeConst.YN.N);

        result.setProjCondCdVal(commonCodeApplication.getCodeName(result.getProjCondCd()));
        result.setProjWorkLocatVal(commonCodeApplication.getCodeName(result.getProjWorkLocat()));
        result.setPitchProjTypeCdVal(commonCodeApplication.getCodeName(result.getPitchProjTypeCd()));
        result.setProjIndivCompTypeVal(commonCodeApplication.getCodeName(result.getProjIndivCompType()));

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }


    /**
     * select project one of project projId
     * @param projId
     * @return
     */
    //TODO: will remove a code
    public Response<List<ProjectMetaDataDTO>> getProjectMetadata(Long projId) {
        logger.info("##### getProjectList #####");
        Response<List<ProjectMetaDataDTO>> res = new Response<>();

        List<ProjectMetaDataDTO> metadataList = projectMetadataRepository.findProjectMetadataByProjId(projId).stream()
                .map(e -> modelMapper.map(e, ProjectMetaDataDTO.class))
                .collect(Collectors.toList());
        metadataList = this.metadataSetting(metadataList);
//        List<String> roleDcodes = metadataList.stream()
//                .filter(e -> StringUtils.equals(e.getAttrDtlCd().substring(0,3),"DEX"))
//                .map(f -> f.getAttrDtlCd()).collect(Collectors.toList());
//        List<String> lanDcodes = metadataList.stream()
//                .filter(e -> StringUtils.equals(e.getAttrDtlCd().substring(0,3),"LAN"))
//                .map(f -> f.getAttrDtlCd()).collect(Collectors.toList());
//        List<String> songDcodes = metadataList.stream()
//                .filter(e -> !(StringUtils.equals(e.getAttrDtlCd().substring(0,3),"DEX") || StringUtils.equals(e.getAttrDtlCd().substring(0,3),"LAN")))
//                .map(f -> f.getAttrDtlCd()).collect(Collectors.toList());
//
//
//        List<AttrDtlCdResponse> role = commonCodeApplication.getRoleValList(roleDcodes);
//        metadataList.stream().forEach(f -> role.stream().forEach(g -> {
//            if(StringUtils.equals(f.getAttrDtlCd(), g.getDcode())) {
//                f.setAttrDtlCdVal(g.getVal());
//            }
//        }));
//
//        List<AttrDtlCdResponse> song = commonCodeApplication.getSongCdValList(songDcodes);
//        metadataList.stream().forEach(f -> song.stream().forEach(g -> {
//            if(StringUtils.equals(f.getAttrDtlCd(), g.getDcode())) {
//                f.setAttrDtlCdVal(g.getVal());
//            }
//        }));
//
//        List<LanguageCdResponse> lang = commonCodeApplication.getLanguageValList(lanDcodes);
//        metadataList.stream().forEach(f -> lang.stream().forEach(g -> {
//            if(StringUtils.equals(f.getAttrDtlCd(), g.getLanguageCd())) {
//                f.setAttrDtlCdVal(g.getLanguage());
//            }
//        }));

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(metadataList);
        return res;
    }

    private List<ProjectMetaDataDTO> metadataSetting(List<ProjectMetaDataDTO> metadataList) {
        logger.info("<<<<< metadataSetting {}",metadataList );

        if(metadataList != null && metadataList.size() > 0) {
            List<String> roleDcodes = metadataList.stream()
                    .filter(e -> StringUtils.equals(e.getAttrDtlCd().substring(0,3),"DEX"))
                    .map(f -> f.getAttrDtlCd()).collect(Collectors.toList());
            List<String> lanDcodes = metadataList.stream()
                    .filter(e -> StringUtils.equals(e.getAttrDtlCd().substring(0,3),"LAN"))
                    .map(f -> f.getAttrDtlCd()).collect(Collectors.toList());
            List<String> songDcodes = metadataList.stream()
                    .filter(e -> !(StringUtils.equals(e.getAttrDtlCd().substring(0,3),"DEX") || StringUtils.equals(e.getAttrDtlCd().substring(0,3),"LAN")))
                    .map(f -> f.getAttrDtlCd()).collect(Collectors.toList());


            List<AttrDtlCdResponse> role = commonCodeApplication.getRoleValList(roleDcodes);
            metadataList.stream().forEach(f -> role.stream().forEach(g -> {
                if(StringUtils.equals(f.getAttrDtlCd(), g.getDcode())) {
                    f.setAttrDtlCdVal(g.getVal());
                }
            }));

            List<AttrDtlCdResponse> song = commonCodeApplication.getSongCdValList(songDcodes);
            metadataList.stream().forEach(f -> song.stream().forEach(g -> {
                if(StringUtils.equals(f.getAttrDtlCd(), g.getDcode())) {
                    f.setAttrDtlCdVal(g.getVal());
                }
            }));

            List<LanguageCdResponse> lang = commonCodeApplication.getLanguageValList(lanDcodes);
            metadataList.stream().forEach(f -> lang.stream().forEach(g -> {
                if(StringUtils.equals(f.getAttrDtlCd(), g.getLanguageCd())) {
                    f.setAttrDtlCdVal(g.getLanguage());
                }
            }));
        }

        return metadataList;
    }

    /**
     * select reference info
     * @param projId
     * @return
     */
    public Response<List<ProjectReferenceFileDTO>> getProjectReferenceFileList(Long projId) {
        logger.info("##### projectReferenceFileDTOResponse #####");
        Response<List<ProjectReferenceFileDTO>> res = new Response<>();

        Optional<Project> findId = projectRepository.findById(projId);
        if(findId.isEmpty()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<ProjectReferenceFileDTO> result = projectReferenceFileRepository.findProjectReferenceFilesByProjId(projId).stream()
                .map(e -> modelMapper.map(e, ProjectReferenceFileDTO.class))
                .collect(Collectors.toList());

        List<String> dcodes = result.stream().map(f -> f.getUploadTypeCd()).collect(Collectors.toList());
        List<AttrDtlCdResponse> meta = commonCodeApplication.getAttrDtlCdValList(dcodes);
        result.stream().forEach(f -> meta.stream().forEach(g -> {
            if(StringUtils.equals(f.getUploadTypeCd(), g.getDcode())) {
                f.setUploadTypeCdVal(g.getVal());
            }
        }));


        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);
        return res;
    }

    /**
     * update Project
     * @param dto
     * @return
     */
    @Transactional
    public Response<Void> updateProjectDetail(ProjectModifyRequest dto) {
        logger.info("##### getProjectUpdate #####");
        Response<Void> res = new Response<>();

        if(dto == null || dto.getProjId() == null || dto.getProjId() == 0L) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        try {
            ProjectBaseDTO baseDTO = modelMapper.map(dto, ProjectBaseDTO.class);
            projectRepository.projectRepositoryDynamicUpdate(baseDTO);
            // TODO: metadata update
            updateMetadata(dto.getMetadataList(),dto.getProjId());
            // TODO: fileUpload update
            updateFiles(dto.getFiles(), dto.getProjId());

        } catch (TransactionalException e){
            logger.error("TransactionalException >>>>> {}", e.getMessage());
        } catch (Exception e){
            logger.error("### PROJECT UPDATE ERROR ###");
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * update Project
     * @return
     */
    @Transactional
    public Response<Void> projectOwnerChange(Long projId, Long projOwner) {
        logger.info("##### projectOwnerChange #####");
        Response<Void> res = new Response<>();

        if(projId == null || projId == 0L) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        // onwer change 체크
        if(commonCodeApplication.countProjectPitchedSong(projId)) {
            res.setResultCd(ResultCodeConst.NOT_AVAILABLE_OWNER_CHANGE.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        ProjectBaseDTO dto = ProjectBaseDTO.builder()
                .projId(projId)
                .projOwner(projOwner)
                .build();

        try {
            ProjectBaseDTO baseDTO = modelMapper.map(dto, ProjectBaseDTO.class);
            long result = projectRepository.updateprojectOwnerChange(baseDTO);
            if(result > 0) { // update 성공시에만
                logger.info("result : {}", result);
                commonCodeApplication.updateSongOwnerChange(projOwner, projId);
            }
        } catch (TransactionalException e){
            logger.error("TransactionalException >>>>> {}", e.getMessage());
        } catch (Exception e){
            logger.error("### PROJECT UPDATE ERROR ### {} ", e.getMessage());
            res.setResultCd(ResultCodeConst.FAIL.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    /**
     *
     * @param dto
     * @return
     */
    @Transactional
    public Response<Void> updateProjectFiles(ProjectReferenceFilesModifyRequest dto) {
        Response<Void> res = new Response<>();

        try {
            // TODO: fileUpload update
            updateFiles(dto.getFiles(), dto.getProjId());
        } catch (Exception e){
            logger.error("##### UPDATE FAIL FILES");
            res.setResultCd(ResultCodeConst.ERROR.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        }
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));

        return res;
    }


    /**
     * updateFiles(insert, delete, update)
     * @param dto
     * @param projId projId !=0 then insert, projId == 0 is update
     */
    private void updateFiles(List<ProjectReferenceFileModifyRequest> dto, Long projId){
        logger.info("<<<<< updateFiles {} ", dto);
        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);
        if(dto == null || dto.size() < 0) {
            logger.info("#### updateFiles Files is not found ####");
            return;
        }

        List<ProjectReferenceFile> list = projectReferenceFileRepository.findProjectReferenceFilesByProjId(projId);
        if(list.size() > 0 ){
            boolean [] listRemove = new boolean[list.size()];
//            List<ProjectReferenceFileDTO> files =  dto.getFiles();

            for(int i=0 ; i<list.size() ; i++) {
                for(int j=0 ; j<dto.size() ; j++) {
                    if(list.get(i).getFileFefSeq().equals(dto.get(j).getFileFefSeq())) {
                        listRemove[i] = true;
                    }
                }
            }

            //TODO: delete metadata by projectId
            try {
                for(int i=0 ; i<list.size() ; i++) {
                    if(!listRemove[i]) {
                        if(list.get(i).getFileFefSeq() > 0) {
                            projectReferenceFileRepository.deleteByProjIdAndFileFefSeq(projId, list.get(i).getFileFefSeq());
                        } else {
                            projectReferenceFileRepository.deleteByProjIdAndRefUrl(projId, list.get(i).getRefUrl());
                        }
                    }
                }
            } catch (TransactionalException e){
                logger.error("TransactionalException >>>>> {}", e.getMessage());
                return ;
            }
        }


        // Upload Files
        Long finalProjectId = projId;
        dto.stream().forEach(fileObj -> {
            ProjectReferenceFileDTO vo = modelMapper.map(fileObj, ProjectReferenceFileDTO.class);
            vo.setProjId(finalProjectId);

            // save
            if(fileObj.getFileFefSeq() == null || fileObj.getFileFefSeq() == 0L){
                vo.setRegUsr(userInfoDTO.getLastUseProfileId());
                vo.setRegDt(LocalDateTime.now());
                vo.setModUsr(userInfoDTO.getLastUseProfileId());
                vo.setModDt(LocalDateTime.now());
                ProjectReferenceFile entity = modelMapper.map(vo, ProjectReferenceFile.class);
                projectReferenceFileRepository.save(entity);
            } else {
                // modify
//                vo.setFileFefSeq(fileObj.getFileFefSeq());
                projectReferenceFileRepository.projectReferenceFileModify(vo);
            }


        });

    }

    /**
     * Metadata update(insert, delete, update)
     * @param metadataList
     * @param projId projId !=0 then insert, projId == 0 is update
     */
    private void updateMetadata(String metadataList, Long projId){
        logger.info("<<<<< updataMetadata {}", metadataList);
        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);
        Long projectId = projId;
        //TODO: metadata list valid 필요
        //metadata modify
        //TODO: delete metadata by projectId
        try {
            projectMetadataRepository.deleteByProjId(projectId);
        } catch (TransactionalException e){
            logger.error("TransactionalException >>>>> {}", e.getMessage());
            return ;
        }

        if(!StringUtils.isEmpty(metadataList)) {
            // save
            Arrays.stream(metadataList.split(",")).forEach(e -> {
                ProjectMetadata vo = ProjectMetadata.builder()
                        .projId(projectId)
                        .attrTypeCd(e.substring(0,3))
                        .attrDtlCd(e)
                        .regUsr(userInfoDTO.getLastUseProfileId())
                        .modUsr(userInfoDTO.getLastUseProfileId())
                        .regDt(LocalDateTime.now())
                        .modDt(LocalDateTime.now())
                        .build();
                projectMetadataRepository.save(vo);
            });
        }

    }

    @Transactional
    public Response<Void> updateProjectBookmark(ProjectBookmarkRequest req) {
        Response<Void> res = new Response<>();

        // project Id Yn 처리
        if(projectRepository.findById(req.getProjId()).isEmpty()){
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        ProjectBookmark result = projectBookmarkRepository.findProjectBookmarkByProjIdAndProfileId(req.getProjId(), req.getProfileId());
        if(result == null){
            //insert
            ProjectBookmark bookmark = ProjectBookmark.builder()
                    .projId(req.getProjId())
                    .profileId(req.getProfileId())
                    .bookmarkYn(CommonCodeConst.YN.Y)
                    .regUsr(req.getProfileId())
                    .regDt(LocalDateTime.now())
                    .modUsr(req.getProfileId())
                    .modDt(LocalDateTime.now())
                    .build();
            projectBookmarkRepository.save(bookmark);
        } else {
            req.setBookmarkYn(StringUtils.equals(result.getBookmarkYn(), CommonCodeConst.YN.Y) ? CommonCodeConst.YN.N : CommonCodeConst.YN.Y);
            projectBookmarkRepository.projectBookmarkDynamicUpdate(req);
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response<Void> testFile(ProjectReferenceFileDTO projectReferenceFileDTO){
        Response<Void> res = new Response<>();
        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);
        ProjectReferenceFile vo = ProjectReferenceFile.builder()
                .projId(261L)
                .uploadTypeCd(projectReferenceFileDTO.getUploadTypeCd())
                .fileName(projectReferenceFileDTO.getFileName())
                .filePath(projectReferenceFileDTO.getFilePath())
                .fileSize(projectReferenceFileDTO.getFileSize())
                .refUrl(projectReferenceFileDTO.getRefUrl())
                .modUsr(userInfoDTO.getLastUseProfileId())
                .modDt(LocalDateTime.now())
                .build();

        vo.setRegUsr(userInfoDTO.getLastUseProfileId());
        vo.setRegDt(LocalDateTime.now());

        projectReferenceFileRepository.save(vo);
        return  res;
    }

}
