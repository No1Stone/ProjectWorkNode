package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.jpa.entity.ProjectReferenceFile;
import com.ui.projectworknode.project.jpa.repository.dsl.ProjectReferenceFileDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectReferenceFileRepository extends JpaRepository<ProjectReferenceFile, Long> , ProjectReferenceFileDynamic {

    List<ProjectReferenceFile> findProjectReferenceFilesByProjId(Long projId);
    int deleteByProjId(Long projId);
    int deleteByProjIdAndFileFefSeq(Long projId,Long fileFefSeq);
    int deleteByProjIdAndRefUrl(Long projId, String refUrl);
}