package com.ui.projectworknode.project.biz.v1.project.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectBookmarkRequest {

    @ApiModelProperty(name = "projId", required = true, notes = "bookmark할 프로젝트 ID")
    @Schema(name = "projId", description = "project id", required = true)
    private Long projId;

    @ApiModelProperty(name = "profileId", required = true, notes = "bookmark하는 회원 ID")
    @Schema(name = "profileId", description = "profileId id, 추후 profile개발 방향 잡히면 추가 수정 필요", required = true)
    private Long profileId;

    @ApiModelProperty(name = "bookmarkYn", required = false, notes = "값 X")
    @Schema(name = "bookmarkYn", description = "Y or N", required = false)
    private String bookmarkYn;
}
