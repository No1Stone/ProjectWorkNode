package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.jpa.entity.ProjectMemb;
import com.ui.projectworknode.project.jpa.repository.dsl.ProjectMembRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ProjectMembRepository extends JpaRepository<ProjectMemb, Long>, ProjectMembRepositoryDynamic {
    ProjectMemb findProjectMembByProjIdAndProfileId(Long projId, Long profileId);

    @Transactional
    void deleteProjectMembByProjIdAndProfileId(Long projId, Long profileId);
}