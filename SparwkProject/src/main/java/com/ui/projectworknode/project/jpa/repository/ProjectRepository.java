package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.biz.v1.project.dto.project.ProjectCompleteYnResponse;
import com.ui.projectworknode.project.jpa.entity.Project;
import com.ui.projectworknode.project.jpa.repository.dsl.ProjectRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long>, ProjectRepositoryDynamic {


    @Query(value = "SELECT nextval('tb_project_seq')", nativeQuery = true)
    Long newProjectSeq();
    List<Project> findProjectsByProjOwner(long projOwner);
    Project findProjectByProjId(long projId);



    @Query(nativeQuery = true, value = "select\n" +
            " case when songCnt > 0 then\n" +
            "     case when songCnt = availSongCnt then 'Y' ELSE 'N'\n " +
            "         END\n" +
            "     END as completeYn " +
            "from(\n" +
            "select\n" +
            "    proj_id\n" +
            "     ,count(A.song_id) over(partition by proj_id) as songCnt\n" +
            "     ,count((select B.song_avail_yn from tb_song C where C.song_avail_yn = 'Y' and C.song_id = B.song_id)) over(partition by proj_id) as availSongCnt\n" +
            "from tb_project_song A\n" +
            "         inner join tb_song B on A.song_id = B.song_id\n" +
            "where proj_id = :projId \n" +
            "limit 1\n" +
            "    ) A " )
    ProjectCompleteYnResponse findProjIdAndSongCntAndAvailSongCntByProjId(
            @Param("projId") Long projId
    );
}