package com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectReferenceFilesModifyRequest {

    @ApiModelProperty(name = "projId", example = "1", value = "projId")
    private Long projId;
    private List<ProjectReferenceFileModifyRequest> files;

}
