package com.ui.projectworknode.project.constants;

public class CommonCodeConst {

	public final static class YN {
		public final static String Y = "Y";
		public final static String N = "N";
	}

	public final static class ADMIN_CODE {
		public final static String [] COMMONS = {
				"MED","HSN","MSG","PJT","ENC","UPL","ACT","SVR","IND"
				,"NOT","ISO","BSL","SSK","HSC","SSF","SIC","TAP","TIL"
				,"PWL","COP","HED","SGN","RYL","PJC","SAV","GND","COF"
				,"ANR","SNS","SST","SGK","SMK"
		};
		public final static String [] ROLES = {
				"ATS","BSN","SPK","RHD","ISW","DEX","ISR"
		};
		public final static String [] SONGS = {
				"SGG","SGS","SGA","SGT","SGE","SGP","SGM","SGI","SGV","SGC"
		};
	}

	public final static class PROJ_INDIV_COMP_TYPE {
		public final static String INDIVIDUAL 	= "IND000001";
		public final static String COMPANY 		= "IND000002";
	}


	public final static class PROJ_WORK_LOCAT {
		public final static String ON_SITE = "PWL000001";
		public final static String BY_ONLINE = "PWL000002";
	}

	public final static class PROJ_COND_CD {
		public final static String TERMS 		= "PJC000001";
		public final static String DISTRIBUTION = "PJC000002";
		public final static String BUDGET 		= "PJC000003";
	}
}