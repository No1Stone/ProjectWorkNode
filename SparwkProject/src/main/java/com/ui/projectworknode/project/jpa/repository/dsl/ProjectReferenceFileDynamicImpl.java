package com.ui.projectworknode.project.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileDTO;
import com.ui.projectworknode.project.jpa.entity.QProjectReferenceFile;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProjectReferenceFileDynamicImpl implements ProjectReferenceFileDynamic{
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProjectReferenceFileDynamicImpl.class);

    private final QProjectReferenceFile qProjectReferenceFile = QProjectReferenceFile.projectReferenceFile;

    private ProjectReferenceFileDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public long projectReferenceFileModify(ProjectReferenceFileDTO dto) {
        logger.info("### projectReferenceFileModify ### {} ", dto);

        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProjectReferenceFile);

        if (!StringUtils.isEmpty(dto.getFileName())) {
            jpaUpdateClause.set(qProjectReferenceFile.fileName, dto.getFileName());
        }
        if (!StringUtils.isEmpty(dto.getFilePath())) {
            jpaUpdateClause.set(qProjectReferenceFile.filePath, dto.getFilePath());
        }
        if (dto.getFileSize() != 0) {
            jpaUpdateClause.set(qProjectReferenceFile.fileSize, dto.getFileSize());
        }
        if (!StringUtils.isEmpty(dto.getRefUrl())) {
            jpaUpdateClause.set(qProjectReferenceFile.refUrl, dto.getRefUrl());
        }


//        jpaUpdateClause.set(qProjectReferenceFile.modUsr, dto.getModUsr());
//        jpaUpdateClause.set(qProjectReferenceFile.modDt, LocalDateTime.now());

        jpaUpdateClause.where(qProjectReferenceFile.fileFefSeq.eq(dto.getFileFefSeq()));

        long result = 0;
        result = jpaUpdateClause.execute();
        //TODO : result 값에 따라 res 결과 반환
        em.flush();
        em.clear();

        return result;
    }

}
