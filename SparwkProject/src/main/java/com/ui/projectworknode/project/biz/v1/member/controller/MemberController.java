package com.ui.projectworknode.project.biz.v1.member.controller;

import com.ui.projectworknode.project.biz.v1.member.dto.ProjectMembLeaveRequest;
import com.ui.projectworknode.project.biz.v1.member.dto.ProjectMembParticipateRequest;
import com.ui.projectworknode.project.biz.v1.member.dto.ProjectMembRequest;
import com.ui.projectworknode.project.biz.v1.member.dto.ProjectMembResponse;
import com.ui.projectworknode.project.biz.v1.member.service.ProjectMemberService;
import com.ui.projectworknode.project.config.common.Environment;
import com.ui.projectworknode.project.config.common.Response;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = Environment.API_VERSION + "/memb")
@Api(tags = "Project Member API")
public class MemberController {

    private final Logger logger = LoggerFactory.getLogger(MemberController.class);
    private final ProjectMemberService projectMemberService;



    @ApiOperation(
            value = "project 멤버 초대/지원 - 프로젝트 멤버 초대/지원, 여러명 or 1명 가능"
            ,notes = "tb_project, tb_project_memb"
            ,tags = {
                    "(0900) Project detail (for Owner)"
                    ,"(0903) RSVP Info (for Owner)"
            }
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5010, message = "프로젝트 ID가 없습니다."),
    })
    @PostMapping(path = "/participate")
    public Response<Void> projectMembParticipate(
            @RequestBody ProjectMembParticipateRequest projectMembParticipateRequest
    ) {
        logger.info("###### CONTROLLER :: projectMembParticipate ######");
        return projectMemberService.saveProjectMemb(projectMembParticipateRequest);
    }


    @ApiOperation(
            value = "select project member list"
            ,notes = "tb_project\n" +
                     ",tb_project_memb"
            ,tags = {
                    "(0903) RSVP Info (for Owner)"
                    ,"(1010) Song > Song & Lyrics"
                    ,"(1002) Project > Co-Writer"
            }
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projId", value = "project id", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=5120, message = "project Id가 존재하지 않음"),
            @ApiResponse(code=9999, message = "ERROR")
    })
    @PostMapping(path = "/list/{projId}")
    public Response<ProjectMembResponse> projectMembList(
            @PathVariable(name = "projId") long projId
    ) {
        logger.info("###### ProjectMembRequestDTO ######");
        return projectMemberService.projectMembList(projId);
    }

    @ApiOperation(
            value = "select project member list"
            ,notes = "tb_project\n" +
            ",tb_project_memb"
            ,tags = {"(0903) RSVP Info (for Owner)"}
    )
    @PostMapping(path = "/confirm")
    public Response<Void> projectMemberConfirm(
            @RequestBody ProjectMembRequest projectMembRequest
    ) {
        logger.info("###### ProjectMembRequestDTO ######");
        return projectMemberService.projectMemberConfirm(projectMembRequest);
    }


    @ApiOperation(
            value = "leave project, 5100 : NOT FOUND INFO , 5400 : 프로젝트 leave 불가"
            ,notes = "tb_project\n" +
            ",tb_project_memb"
            ,tags = {"(0900) Project detail (for Owner)"}
    )
    @PostMapping(path = "/leave")
    public Response<Void> projectMemberLeave(
            @RequestBody ProjectMembLeaveRequest projectMembLeaveRequest
    ) {
        logger.info("###### projectMemberLeave ######");
        return projectMemberService.projectMemberLeave(projectMembLeaveRequest);
    }

}

