package com.ui.projectworknode.project.biz.v1.project.dto.project;

import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileDTO;
import com.ui.projectworknode.project.jpa.entity.Project;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.groups.Default;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectRequest {

    @ApiModelProperty
    @Schema(name = "project", description = "project object")
    private Project project;

    @ApiModelProperty
    @Schema(name = "profileId", description = "profileId")
    private Long profileId;
    /* target table service */
    @Schema(name = "serviceCntrCdList", description = "project save service list")
    private String serviceCntrCdList;
    /* target table tb_project_metadata */
    @Schema(name = "rolesCd", description = "project role")
    private String rolesCd;
    @Schema(name = "rolesList", description = "rolesList")
    private String rolesList;
    @Schema(name = "genresCd", description = "genresCd")
    private String genresCd;
    @Schema(name = "genresList", description = "genresList")
    private String genresList;
    @Schema(name = "moodsCd", description = "moodsCd")
    private String moodsCd;
    @Schema(name = "moodsList", description = "moodsList")
    private String moodsList;
    @Schema(name = "themesCd", description = "themesCd")
    private String themesCd;
    @Schema(name = "themesList", description = "themesList")
    private String themesList;
    /* target table tb_project_reference_file */
    @Schema(name = "files", description = "files")
    private List<ProjectReferenceFileDTO> files;

    public interface ProjectListValidation extends Default {}
//    public interface LoginValidation extends Default {}
//    public interface JoinValidation extends Default {}
}
