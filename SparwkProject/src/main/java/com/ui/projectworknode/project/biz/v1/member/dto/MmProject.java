package com.ui.projectworknode.project.biz.v1.member.dto;

import lombok.Data;

@Data
public class MmProject {

    private String me;
    private String teamId;

}
