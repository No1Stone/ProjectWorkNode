package com.ui.projectworknode.project.constants;

public enum ResultCodeConst {
    //공통처리
    SUCCESS("0000"),

    //project


    //valid
    NOT_FOUND_PROJECT_INFO("5100"),
    NOT_EXIST_PROFILE_ID("5000"),
    NOT_FOUND_PROJECT_ID("5010"),
    NOT_VALID_COMPANY_CD("5200"),
    NOT_MATCH_PROJECT_OWNER("5210"),
    NOT_AVAILABLE_OWNER_CHANGE("5300"),
    NOT_AVAILABLE_LEAVE_PROJECT("5400"),


    //Fail
    FAIL("2000"),

    //System Error
    ERROR("9999");
    private String code;

    ResultCodeConst(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
