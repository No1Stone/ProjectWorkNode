package com.ui.projectworknode.project.biz.v1.project.dto.metadata;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMetadataBaseDTO {
    private Long projId;
    private String projAttrType;
    private String projAttrId;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
