package com.ui.projectworknode.project.constants;

public class ProjectMembConst {
	public enum PARTICIPATE {
		// 초대
		RSVP,
		// 추천
		APPLY;
	}

	public enum LEAVE {
		// 하차
		QUIT,
		// 강퇴
		BAN;
	}

	public enum APPLY_STAT {

//		A(apply, 지원) / C(cancel, 취소) / P(in progress, 처리중) / H(hold off, 보류)
		APPLY("A"),
		CANCEL("C"),
		IN_PROGRESS("P"),
		HOLD_OFF("H");

		private String code;
		APPLY_STAT(String code) {
			this.code = code;
		}
		public String getCode() {
			return code;
		}
	}
	public enum INVT_STAT {
//		A(approval, 승인) / R(reject, 거절) / P(in progress, 처리중) / H(hold off, 보류)
		APPROVAL("A"),
		REJECT("C"),
		IN_PROGRESS("P"),
		HOLD_OFF("H");

		private String code;
		INVT_STAT(String code) {
			this.code = code;
		}
		public String getCode() {
			return code;
		}
	}

}