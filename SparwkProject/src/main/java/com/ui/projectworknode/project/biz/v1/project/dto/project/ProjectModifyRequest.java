package com.ui.projectworknode.project.biz.v1.project.dto.project;

import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileModifyRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectModifyRequest {

    @ApiModelProperty(name = "projId", example = "1", notes = "project Id", value = "projSeq", required = true)
    private Long projId;

    @ApiModelProperty(name = "projOwner", example = "10000000198", notes = "project Owner",value = "projOwnerId", required = true)
    private Long projOwner;

    @ApiModelProperty(name = "projWhatFor", example = "project what for", value = "projWhatFor")
    private String projWhatFor;

    @ApiModelProperty(name = "projWorkLocat", example = "PWL000001", value = "projWorkLocat")
    private String projWorkLocat;

    @ApiModelProperty(name = "projCondCd", example = "PJC000001", value = "projCondCd")
    private String projCondCd;

    @ApiModelProperty(name = "recruitYn", example = "N", value = "recruitYn")
    private String recruitYn;

    @ApiModelProperty(name = "leaveYn", example = "N", value = "leaveYn")
    private String leaveYn;

    @ApiModelProperty(name = "minVal", example = "0", value = "minVal")
    private Long minVal = 0L;
    @ApiModelProperty(name = "maxVal", example = "0", value = "maxVal")
    private Long maxVal = 0L;

    @ApiModelProperty(name = "metadataList", example = "SGT000087,SGE000006,SGG000039,SGP000008,DEX000017,SGT001265,SGS000235,SGP000002,SGG000029", value = "metadataList")
    private String metadataList;
    /* target table tb_project_reference_file */
    private List<ProjectReferenceFileModifyRequest> files;
}
