package com.ui.projectworknode.project.biz.v1.member.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectMembLeaveRequest {

    @ApiModelProperty(name = "projId", value = "프로젝트고유ID", notes = "프로젝트 고유 ID")
    @Schema(name = "projId", description = "project object")
    private Long projId;

    @ApiModelProperty(name = "profileId", value = "멤버list", notes = "profileList" , example = "1")
    @Schema(name = "profileId", description = "project member profileId")
    private Long profileId;

    @ApiModelProperty(name = "leaveType", value = "하차/강퇴", notes = "프로잭트 member 하차/강퇴" , example = "QUIT/BAN")
    @Schema(name = "leaveType", description = "project member Leave type", example = "QUIT/BAN")
    private String leaveType;
}
