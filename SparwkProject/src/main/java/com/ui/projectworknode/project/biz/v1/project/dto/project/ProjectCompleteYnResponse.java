package com.ui.projectworknode.project.biz.v1.project.dto.project;

public interface ProjectCompleteYnResponse {

    String getCompleteYn();
}
