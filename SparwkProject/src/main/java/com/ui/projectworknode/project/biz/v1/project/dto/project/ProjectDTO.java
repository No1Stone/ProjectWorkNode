package com.ui.projectworknode.project.biz.v1.project.dto.project;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectDTO {
    private Long projId;
    private Long projOwner;
    private String projAvalYn;
    private String genderCd;
    private String projIndivCompType;
    private String projPublYn;
    private String projPwd;
    private String projInvtTitle;
    private String projInvtDesc;
    private String projWhatFor;
    private String pitchProjTypeCd;
    private String projWorkLocat;
    private String projTitle;
    private String projDesc;
    private LocalDate projDdlDt;
    private String projCondCd;
    private Long minVal;
    private Long maxVal;
    private String recruitYn;
    private String avatarFileUrl;
    private String leaveYn;
    private String completeYn;
    private Long compProfileId;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
