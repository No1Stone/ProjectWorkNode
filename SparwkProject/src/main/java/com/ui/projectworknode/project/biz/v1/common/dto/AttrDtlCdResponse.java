package com.ui.projectworknode.project.biz.v1.common.dto;

public interface AttrDtlCdResponse {

    String getDcode();
    String getVal();
}
