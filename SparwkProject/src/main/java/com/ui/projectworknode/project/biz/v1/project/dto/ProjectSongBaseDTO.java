package com.ui.projectworknode.project.biz.v1.project.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectSongBaseDTO {
    private Long projId;
    private Long songId;
}
