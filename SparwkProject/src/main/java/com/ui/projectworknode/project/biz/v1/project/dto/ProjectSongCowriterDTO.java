package com.ui.projectworknode.project.biz.v1.project.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectSongCowriterDTO {
    private Long projId;
    private Long songId;
    private Long profileId;
    private double rateShare;
    private String acceptYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
