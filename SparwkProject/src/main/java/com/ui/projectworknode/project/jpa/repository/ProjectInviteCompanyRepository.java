package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.jpa.entity.ProjectInviteCompany;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectInviteCompanyRepository extends JpaRepository<ProjectInviteCompany, Long>{
}