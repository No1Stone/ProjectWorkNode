package com.ui.projectworknode.project.biz.v1.song.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectSongDTO {

    private long projId;
    private long songId;
    private String useYn;

}
