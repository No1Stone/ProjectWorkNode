package com.ui.projectworknode.project.biz.v1.project.dto.metadata;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class ProjectMetaResponse {
        private String projId;
        private String attrTypeCd;
        private String attrDtlCd;
        private String attrDtlCdVal;
}
