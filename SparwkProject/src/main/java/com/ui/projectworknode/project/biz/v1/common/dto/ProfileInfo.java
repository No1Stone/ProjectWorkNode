package com.ui.projectworknode.project.biz.v1.common.dto;

public interface ProfileInfo {

    Long getProfileId();
    String getFullName();
    String getProfileImgUrl();
    String getHeadline();
    String getHireMeYn();

    String getBio();
    String getCurrentCityCountryCd();
    String getCurrentCityCountryNm();
    String getCurrentCityNm();
    String getHomeTownCountryCd();

    String getCompanyVerifyYn();
    String getCreateYn();
    String getArtistYn();
    String getAnrYn();

    Long getCompanyProfileId();
    String getPrimaryYn();
    String getProfileCompanyName();
    String getCompanyProfileImgUrl();

}
