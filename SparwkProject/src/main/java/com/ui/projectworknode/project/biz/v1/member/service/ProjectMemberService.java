package com.ui.projectworknode.project.biz.v1.member.service;

import com.google.gson.Gson;
import com.ui.projectworknode.project.biz.v1.common.CommonCodeApplication;
import com.ui.projectworknode.project.biz.v1.member.dto.*;
import com.ui.projectworknode.project.config.common.Response;
import com.ui.projectworknode.project.config.common.RestCall;
import com.ui.projectworknode.project.constants.CommonCodeConst;
import com.ui.projectworknode.project.constants.ProjectMembConst;
import com.ui.projectworknode.project.constants.ResultCodeConst;
import com.ui.projectworknode.project.jpa.entity.Project;
import com.ui.projectworknode.project.jpa.entity.ProjectMemb;
import com.ui.projectworknode.project.jpa.repository.ProjectMembRepository;
import com.ui.projectworknode.project.jpa.repository.ProjectRepository;
import com.ui.projectworknode.project.jpa.repository.ProjectSongRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProjectMemberService {

    private final Logger logger = LoggerFactory.getLogger(ProjectMemberService.class);

    private final ModelMapper modelMapper;

    private final ProjectRepository projectRepository;
    private final ProjectMembRepository projectMembRepository;
    private final MessageSourceAccessor messageSourceAccessor;
    private final ProjectSongRepository projectSongRepository;

    private final CommonCodeApplication commonCodeApplication;


    @Transactional
    public Response<Void> saveProjectMemb(ProjectMembParticipateRequest req) {
        logger.info("##### SERVICE :: saveProjectMemb #####  IN <<<<<< {}" , req);
        Response<Void> res = new Response<>();

        /**
         * 프로젝트 valid 체크
         */
        if(projectRepository.findById(req.getProjectMember().getProjId()).isEmpty()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_ID.name());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        // TODO: Ban 처리 된 회원에 대한 처리 필요
        String [] profileIds = req.getProjectMember().getProfileIdList().split("\\|");
        try {

            for(int i=0 ; i<profileIds.length ; i++){
                long profileId = Long.parseLong(profileIds[i]);
                // TODO: mod, reg user에 대한 ownerId 처리 -> token 활용해서
                // insert merge
                ProjectMemb existMember = projectMembRepository.findProjectMembByProjIdAndProfileId(req.getProjectMember().getProjId(), profileId);
                logger.debug("### PROJECT EXXIST ### IN <<<<< {}", existMember);

                ProjectMemb projectMemb = ProjectMemb.builder()
                        .projId(req.getProjectMember().getProjId())
                        .profileId(profileId)
                        .activeYn(CommonCodeConst.YN.Y)
                        .confirmYn(CommonCodeConst.YN.N)
                        .banYn(CommonCodeConst.YN.N)
                        .quitYn(CommonCodeConst.YN.N)
                        .modUsr(req.getOwnerProfileId())
                        .modDt(LocalDateTime.now())
                        .build();

                if(existMember == null){
                    projectMemb.setRegDt(LocalDateTime.now());
                    projectMemb.setRegUsr(req.getOwnerProfileId());
                }

                if(StringUtils.equals(req.getType().toUpperCase(), ProjectMembConst.PARTICIPATE.RSVP.name())){
                    projectMemb.setInvtStat(ProjectMembConst.INVT_STAT.IN_PROGRESS.getCode());
                    projectMemb.setInvtDt(LocalDateTime.now());
                    // 지원 항목 null
                    projectMemb.setApplyStat(null);
                } else {
                    projectMemb.setApplyStat(ProjectMembConst.APPLY_STAT.IN_PROGRESS.getCode());
                    // rsvp 항목 null
                    projectMemb.setInvtStat(null);
                    projectMemb.setInvtDt(null);
                }
                projectMembRepository.save(projectMemb);

            }

        } catch (Exception e) {
            logger.error("#### Project Member Insert ERROR ####");
        }

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }



    @Transactional
    public Response<ProjectMembResponse> projectMembList(Long projId) {
        logger.info("##### projectMembList #####");
        Response<ProjectMembResponse> res = new Response<>();

        if(projectRepository.findById(projId).isEmpty()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        List<ProjectMembResponse> list = projectMembRepository.projectMembList(projId);
        if(list == null) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

//        list.stream().forEach(e -> {
//            e.setProfile(commonCodeApplication.findViewUserInfo(e.getProfileId()));
//        });

        StringBuilder confirm = new StringBuilder();
        StringBuilder rsvpProgress = new StringBuilder();
        StringBuilder rsvpApproval = new StringBuilder();
        StringBuilder notConfirm = new StringBuilder();
        for(ProjectMembResponse member : list){
            if(member.getProfileId() != null && member.getProfileId() != 0){

                // 프로젝트 인증이 완료 된 멤버처리
                if(StringUtils.equals(member.getConfirmYn(), CommonCodeConst.YN.Y)){
                    confirm.append(member.getProfileId()+",");
                } else {
                    // RSVP 받은 회원 중 인증이 되지 않은 회원 처리
                    if(StringUtils.equals(member.getInvtStat(), ProjectMembConst.INVT_STAT.IN_PROGRESS.getCode())) {
                        rsvpProgress.append(member.getProfileId()+",");
                    } else if(StringUtils.equals(member.getInvtStat(), ProjectMembConst.INVT_STAT.APPROVAL.getCode())){
                        rsvpApproval.append(member.getProfileId()+",");
                    } else {
                        // Recommend면서 인증되지 않은 멤버
                        notConfirm.append(member.getProfileId()+",");
                    }
                }
            }
        }

        ProjectMembResponse result = ProjectMembResponse.builder()
                .confirm(confirm.toString())
                .notConfirm(notConfirm.toString())
                .rsvpProgress(rsvpProgress.toString())
                .rsvpApproval(rsvpApproval.toString())
                .build();

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        res.setResult(result);

        return res;
    }



    @Transactional
    public Response<Void> projectMemberConfirm(ProjectMembRequest req) {
        logger.info("##### projectMemberConfirm ##### {} ",req);
        Response<Void> res = new Response<>();

        Optional<Project> findProject =  projectRepository.findById(req.getProjId());
        if(findProject.isEmpty()) {
            logger.info("#### SERVICE :: PROJECT IS NOT FOUND  ####");
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }


        Project project = findProject.get();
        //TODO: owner Id로 처리해야 함 ownerId.equals project owner 같을 경우
        /**
         * 완료상태 : APPLY, APPROVAL(RSVP)
         * 진행상태 : IN PROGRESS
         */
        // 다수, 단일 회원의 confirm 처리
        Arrays.stream(req.getProfileIdList().split(",")).forEach(profileId -> {
            ProjectMemb projectMemb = projectMembRepository.findProjectMembByProjIdAndProfileId(req.getProjId(), Long.parseLong(profileId));


            if(StringUtils.equals(project.getProjPublYn(), CommonCodeConst.YN.Y)){
                projectMemb.setConfirmYn(CommonCodeConst.YN.Y);
            } else if(StringUtils.equals(project.getProjPublYn(), CommonCodeConst.YN.N)
                        && (StringUtils.equals(projectMemb.getApplyStat(), ProjectMembConst.APPLY_STAT.APPLY.getCode())
                            || StringUtils.equals(projectMemb.getInvtStat(), ProjectMembConst.INVT_STAT.APPROVAL.getCode())))
            {
                // private :: email 인증 받은 후 password입력 성공 했을 때 해당 로직만 타게
                //확인처리
                projectMemb.setConfirmYn(CommonCodeConst.YN.Y);
            }


            if(!StringUtils.isEmpty(projectMemb.getApplyStat())){
                projectMemb.setApplyStat(ProjectMembConst.APPLY_STAT.APPLY.getCode());
            } else {
                projectMemb.setInvtStat(ProjectMembConst.INVT_STAT.APPROVAL.getCode());
            }
            projectMemb.setPatpSdt(LocalDateTime.now());
            projectMemb.setModDt(LocalDateTime.now());
            projectMemb.setModUsr(Long.parseLong(profileId));

            projectMembRepository.save(projectMemb);

            //오너의 팀아이디
            MmCompany mc = new MmCompany();
            mc.setCompanyId(project.getProjOwner());
            mc.setCompanySparwkId(project.getProjOwner());

            //참여자의 메타모스트 아이디
            MmCompany mcu = new MmCompany();
            mc.setCompanyId(projectMemb.getProfileId());
            
            //메타모스트 등록
            MmProject mmp = new MmProject();
            mmp.setTeamId(mc.getTeamid(mc));
            mmp.setMe(mcu.getCompanyId());
            RestCall.postRes("https://communication.sparwkdev.com/cowork/V1"+"/add/team/member",new Gson().toJson(mmp));


        });

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }


    @Transactional
    public Response<Void> projectMemberLeave(ProjectMembLeaveRequest req) {
        logger.info("##### projectMemberLeave ##### {} ",req);
        Response<Void> res = new Response<>();

        if(projectRepository.findById(req.getProjId()).isEmpty()) {
            res.setResultCd(ResultCodeConst.NOT_FOUND_PROJECT_INFO.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
        
        // 떠나려는 회원이 song split 참여시 leave 불가
        if(!commonCodeApplication.cowriterSplitRateSum(req.getProjId(), req.getProfileId())) {
            res.setResultCd(ResultCodeConst.NOT_AVAILABLE_LEAVE_PROJECT.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }

        projectMembRepository.deleteProjectMembByProjIdAndProfileId(req.getProjId(), req.getProfileId());
        commonCodeApplication.deleteSongCowriter(req.getProjId(), req.getProfileId());
        commonCodeApplication.deleteSongCredit(req.getProjId(), req.getProfileId());
//        ProjectMemb projectMemb = projectMembRepository.findProjectMembByProjIdAndProfileId(req.getProjId(), req.getProfileId());
//        if(!StringUtils.isEmpty(projectMemb.getApplyStat())){
//            projectMemb.setApplyStat(ProjectMembConst.APPLY_STAT.CANCEL.getCode());
//        } else {
//            projectMemb.setInvtStat(ProjectMembConst.INVT_STAT.REJECT.getCode());
//        }
//
//        projectMemb.setActiveYn(CommonCodeConst.YN.N);
//        projectMemb.setConfirmYn(CommonCodeConst.YN.N);
//        projectMemb.setPatpEdt(LocalDateTime.now());
//
//        if(StringUtils.equals(req.getLeaveType(), ProjectMembConst.LEAVE.QUIT.name())){
//            // 나감 처리
//            projectMemb.setQuitYn(CommonCodeConst.YN.Y);
//            projectMemb.setQuitDt(LocalDateTime.now());
//        } else {
//            // 강퇴 처리
//            projectMemb.setBanYn(CommonCodeConst.YN.Y);
//            projectMemb.setBanDt(LocalDateTime.now());
//        }
//
//        projectMemb.setModDt(LocalDateTime.now());
//        projectMemb.setModUsr(req.getProfileId());
//
//        projectMembRepository.save(projectMemb);

        //오너의 팀아이디
        Long own = projectRepository.findById(req.getProjId()).orElse(null).getProjOwner();
        MmCompany mc = new MmCompany();
        mc.setCompanyId(own);
        mc.setCompanySparwkId(own);

        //참여자의 메타모스트 아이디
        MmCompany mcu = new MmCompany();
        mc.setCompanyId(req.getProfileId());

        //메타모스트 등록
        MmProject mmp = new MmProject();
        mmp.setTeamId(mc.getTeamid(mc));
        mmp.setMe(mcu.getCompanyId());
        RestCall.postRes("https://communication.sparwkdev.com/cowork/V1"+"/delete/team/member",new Gson().toJson(mmp));

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
