package com.ui.projectworknode.project.jpa.repository.dsl;

import com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile.ProjectReferenceFileDTO;

public interface ProjectReferenceFileDynamic {

    long projectReferenceFileModify(ProjectReferenceFileDTO dto);

}
