package com.ui.projectworknode.project.biz.v1.song.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectSongRequest {

    @Schema(description = "프로젝트 ID", nullable = false, example = "100")
    @NonNull
    private long projId;

    @Schema(description = "Song ID", nullable = false, example = "1")
    @NonNull
    private long songId;
    @Schema(description = "Profile ID", nullable = true, example = "1")
    private long profileId;

    @Schema(description = "projOwner ID", nullable = true, example = "1")
    private long projOwner;

    @Schema(description = "지분율", nullable = true, example = "0.123")
    private double rateShare;
    @Schema(description = "프로젝트 수락 YN", nullable = true, example = "Y,N")
    private String acceptYn;
    @Schema(description = "사용 YN", nullable = true, example = "Y,N")
    private String useYn;

    private String profileList;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
