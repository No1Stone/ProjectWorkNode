package com.ui.projectworknode.project.constants;

/**
 * 소셜 구분 코드
 */
public enum SampleEnumCode {

	/**
	 *  일반
	 */
	NORMAL("01"),

	/**
	 * 자동 로그인
	 */
	AUTO("02"),

	/**
	 * 소셜
	 */
	SOCIAL("03"),

	/**
	 * 간편비밀번호
	 *
	 * */
	SIMPLE("04"),

	/**
	 * 생체
	 */
	BIO("05");

	private String code;

	private SampleEnumCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
