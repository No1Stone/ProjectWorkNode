package com.ui.projectworknode.project.constants;

/**
 * UPDATE/DELTE 구분자
 */
public enum CowriterTpEnum {

	/**
	 * accept yn
	 */
	ACCEPT_YN,
	/**
	 * rateshare
	 */
	RATE_SHARE;

}

