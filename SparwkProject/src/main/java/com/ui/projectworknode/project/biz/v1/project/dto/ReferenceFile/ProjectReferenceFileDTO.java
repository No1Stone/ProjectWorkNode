package com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectReferenceFileDTO {

    private Long    projId;
    private Long    fileFefSeq;
    private String  uploadTypeCd;
    private String  uploadTypeCdVal;
    private String  fileName;
    private String  filePath;
    private int     fileSize;
    private String  refUrl;
    private Long    regUsr;
    private LocalDateTime regDt;
    private Long    modUsr;
    private LocalDateTime modDt;

}
