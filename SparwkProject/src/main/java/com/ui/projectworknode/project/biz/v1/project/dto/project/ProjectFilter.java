package com.ui.projectworknode.project.biz.v1.project.dto.project;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectFilter {

    @ApiModelProperty(position = 1 ,name = "offset", required = true)
    private int offset = 0;
    @ApiModelProperty(position = 2 ,name = "limit", required = true)
    private int limit  = 5;
    @ApiModelProperty(position = 3 ,name = "sort", required = false)
    private String sort;
    @ApiModelProperty(position = 4 ,name = "sortDirection", required = false)
    private String sortDirection;
    @ApiModelProperty(position = 5 ,name = "projTitle", required = false)
    private String projTitle;
    @ApiModelProperty(position = 6 ,name = "pitchProjTypeCd", required = false)
    private String pitchProjTypeCd;
    @ApiModelProperty(position = 7 ,name = "genderCd", required = false)
    private String genderCd;
    @ApiModelProperty(position = 8 ,name = "attrDtlCdList", required = false)
    private String attrDtlCdList;
}
