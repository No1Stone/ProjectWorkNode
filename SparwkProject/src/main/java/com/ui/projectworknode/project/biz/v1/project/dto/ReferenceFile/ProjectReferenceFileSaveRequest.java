package com.ui.projectworknode.project.biz.v1.project.dto.ReferenceFile;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectReferenceFileSaveRequest {

    @ApiModelProperty(name = "uploadTypeCd", example = "UPL000001", value = "uploadTypeCd")
    private String  uploadTypeCd;
    @ApiModelProperty(name = "fileName", example = "Luis_Fonsi-Despacito.mp3", value = "fileName")
    private String  fileName;
    @ApiModelProperty(name = "filePath", example = "", value = "filePath")
    private String  filePath;
    @ApiModelProperty(name = "fileSize", example = "4506591", value = "fileSize")
    private int     fileSize;
    @ApiModelProperty(name = "refUrl", example = "https://examewonseokbucket.s3-ap-northeast-2.amazonaws.com/Luis_Fonsi-Despacito.mp3", value = "refUrl")
    private String  refUrl;

}
