package com.ui.projectworknode.project.jpa.repository;

import com.ui.projectworknode.project.jpa.entity.ProjectMetadata;
import com.ui.projectworknode.project.jpa.repository.dsl.ProjectMetadataRepositoryDynamic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectMetadataRepository extends JpaRepository<ProjectMetadata, Long>, ProjectMetadataRepositoryDynamic {

    List<ProjectMetadata> findProjectMetadataByProjId(Long projId);
    int deleteByProjId(Long projId);
}