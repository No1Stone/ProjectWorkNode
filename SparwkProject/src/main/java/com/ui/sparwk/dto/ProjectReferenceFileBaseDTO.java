package com.ui.sparwk.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectReferenceFileBaseDTO {
    private Long projId;
    private Long fileFef_seq;
    private String uploadType_cd;
    private String fileName;
    private String filePath;
    private Long fileSize;
    private String refUrl;

}
