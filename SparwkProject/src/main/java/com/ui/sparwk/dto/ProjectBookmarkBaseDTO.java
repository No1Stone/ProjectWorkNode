package com.ui.sparwk.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectBookmarkBaseDTO {
    private Long profileId;
    private Long projId;
    private String bookmarkYn;

}
