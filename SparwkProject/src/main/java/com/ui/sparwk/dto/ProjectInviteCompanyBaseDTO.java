package com.ui.sparwk.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProjectInviteCompanyBaseDTO {
    private Long projId;
    private String inviteComp_cd;
}
