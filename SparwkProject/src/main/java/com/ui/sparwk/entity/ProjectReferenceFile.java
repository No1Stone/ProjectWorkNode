package com.ui.sparwk.entity;

import com.ui.projectworknode.project.jpa.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project_reference_file")
public class ProjectReferenceFile extends BaseEntity {

    @Id
    @Column(name = "proj_id",  nullable = true) private Long projId;
    @Column(name = "file_fef_seq",  nullable = true) private Long fileFefSeq;
    @Column(name = "upload_type_cd",  nullable = true) private String uploadTypeCd;
    @Column(name = "file_name",  nullable = true) private String fileName;
    @Column(name = "file_path",  nullable = true) private String filePath;
    @Column(name = "file_size",  nullable = true) private Long fileSize;
    @Column(name = "ref_url",  nullable = true) private String refUrl;


    @Builder
    ProjectReferenceFile(
            Long projId,
            Long fileFefSeq,
            String uploadTypeCd,
            String fileName,
            String filePath,
            Long fileSize,
            String refUrl

            ) {
        this.projId=projId;
        this.fileFefSeq=fileFefSeq;
        this.uploadTypeCd=uploadTypeCd;
        this.fileName=fileName;
        this.filePath=filePath;
        this.fileSize=fileSize;
        this.refUrl=refUrl;
    }


}
