package com.ui.sparwk.entity;

import com.ui.projectworknode.project.jpa.entity.BaseEntity;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project_bookmark")
@DynamicUpdate
public class ProjectBookmark extends BaseEntity {

    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "proj_id", nullable = true)
    private Long projId;
    @Column(name = "bookmark_yn", nullable = true)
    private String bookmarkYn;

    @Builder
    ProjectBookmark(
            Long profileId,
            Long projId,
            String bookmarkYn
    ) {
        this.profileId = profileId;
        this.projId = projId;
        this.bookmarkYn = bookmarkYn;
    }

}
