package com.ui.sparwk.entity;

import com.ui.projectworknode.project.jpa.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project_invite_company")
public class ProjectInviteCompany extends BaseEntity {

    @Id
    @Column(name = "proj_id", nullable = true)
    private Long projId;
    @Column(name = "invite_comp_cd", nullable = true)
    private String inviteComp_cd;

    @Builder
    ProjectInviteCompany(
            Long projId,
            String inviteComp_cd
    ) {
        this.projId = projId;
        this.inviteComp_cd = inviteComp_cd;
    }
}
