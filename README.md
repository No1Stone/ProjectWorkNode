# **Development environment configuration**

```
ProjectWorkNode
```

# **Project Structure**
```
com.sparwk.projectWorkNode.project
- biz(비즈니스 서비스)
    - version
        - project
        - song
- config (설정)
    - common****
    - filter
        - exception
- constatns(공통코드)
- jpa
    - entity
    - repository
        - dsl
```


# **Server Architecture**
```
project port: 8025
```

  
# **Database Table List**  
```
-- create project
tb_project
tb_project_metadata
tb_project_invite_company
tb_project_reference_file
tb_project_service_country

tb_project_bookmark

-- invite member
tb_project_memb

-- new song
tb_project_song
tb_project_song_cowriter
tb_project_song_cowriter_hist
```


# ** 응답코드(ResultCodeConst) **
```****
Common 
 - 0xxx
Project 
 - 5xxx
```